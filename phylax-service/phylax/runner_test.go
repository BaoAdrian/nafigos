package phylax

import (
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"

	core "k8s.io/api/core/v1"
)

var testRunner = &Runner{
	MonitoredResource: MonitoredResource{
		ID:                   "xid",
		StartDate:            testStartDate,
		WorkflowDefinitionID: "wdfxid",
		Owner:                "test_user",
		ResourceName:         "test_user-nafigos-say-bqd4sg30ip20n8068ocg-ooooooooooo",
		Namespace:            "wsimpson",
		Cluster:              testCluster,
	},
	TCPPorts:     []core.ServicePort{},
	HTTPPorts:    []core.ServicePort{},
	AuthDisabled: false,
}

var testRunnerRequest = Request{
	Namespace: "test_user",
	BasicRequest: common.BasicRequest{
		User:       testUser,
		RunID:      "xid",
		WorkflowID: "wfd",
		ClusterID:  "bqd4sg30ip20n8068ocg",
	},
	WorkflowDefinition: struct {
		Name       string             `json:"name,omitempty"`
		Type       string             `bson:"type" json:"type,omitempty" yaml:"type"`
		TCPPorts   []core.ServicePort `json:"tcp_ports,omitempty"`
		Repository common.Repository  `json:"repository"`
	}{Name: "foo"},
}

func testCreateRunner(t *testing.T) {
	adapter := common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("Replace", testRunner).Return(nil)

	db = &DB{
		adapter: adapter,
	}

	name := "foo"
	runID := "xid"
	resourceName := common.CreateResourceName(testUser.Username, name, runID)
	testRunner, err := NewRunner(testRunnerRequest)
	assert.NoError(t, err)

	err = db.Save(testRunner)

	assert.NoError(t, err)
	assert.Equal(t, runID, testRunner.ID)
	assert.Equal(t, "wfd", testRunner.WorkflowDefinitionID)
	assert.Equal(t, resourceName, testRunner.ResourceName)
	assert.Equal(t, testCluster, testRunner.Cluster)
	assert.Equal(t, "test_user", testRunner.Namespace)
	assert.Equal(t, testUser.Username, testRunner.Owner)
	assert.NotEmpty(t, testRunner.StartDate)
}

func testGetRunnersForUser(t *testing.T) {
	adapter := common.MockedAdapter{new(mock.Mock)}

	db = &DB{
		adapter: adapter,
	}

	resourceName1 := common.CreateResourceName("test_user", "foo", "xid")
	resourceName2 := common.CreateResourceName("test_user", "bar", "xid2")
	clusterStringMap := map[string]interface{}{
		"id":                "bqd4sg30ip20n8068ocg",
		"name":              "test_cluster",
		"default_namespace": "default",
		"config":            "VGhlIHJlZCBkZWF0aCBoYWQgbG9uZyBkZXZhc3RhdGVkIHRoZSBjb3VudHJ5LiBObyBwZXN0aWxlbmNlIGhhZCBldmVyIGJlZW4gc28gZmF0YWwsIG9yIHNvIGhpZGVvdXMuIEJsb29kIHdhcyBpdHMgQXZhdGFyIGFuZCBpdHMgc2VhbCAtLSB0aGUgbWFkbmVzcyBhbmQgdGhlIGhvcnJvciBvZiBibG9vZC4gVGhlcmUgd2VyZSBzaGFycCBwYWlucywgYW5kIHN1ZGRlbiBkaXp6aW5lc3MsIGFuZCB0aGVuIHByb2Z1c2UgYmxlZWRpbmcgYXQgdGhlIHBvcmVzLCB3aXRoIGRpc3NvbHV0aW9uLiBUaGUgc2NhcmxldCBzdGFpbnMgdXBvbiB0aGUgYm9keSBhbmQgZXNwZWNpYWxseSB1cG9uIHRoZSBmYWNlIG9mIHRoZSB2aWN0aW0sIHdlcmUgdGhlIHBlc3QgYmFuIHdoaWNoIHNodXQgaGltIG91dCBmcm9tIHRoZSBhaWQgYW5kIGZyb20gdGhlIHN5bXBhdGh5IG9mIGhpcyBmZWxsb3ctbWVuLiBBbmQgdGhlIHdob2xlIHNlaXp1cmUsIHByb2dyZXNzLCBhbmQgdGVybWluYXRpb24gb2YgdGhlIGRpc2Vhc2UsIHdlcmUgaW5jaWRlbnRzIG9mIGhhbGYgYW4gaG91ci4KCkJ1dCBQcmluY2UgUHJvc3Blcm8gd2FzIGhhcHB5IGFuZCBkYXVudGxlc3MgYW5kIHNhZ2FjaW91cy4gV2hlbiBoaXMgZG9taW5pb25zIHdlcmUgaGFsZiBkZXBvcHVsYXRlZCwgaGUgc3VtbW9uZWQgdG8gaGlzIHByZXNlbmNlIGEgdGhvdXNhbmQgaGFsZSBhbmQgbGlnaHQtaGVhcnRlZCBmcmllbmRzIGZyb20gYW1vbmcgdGhlIGtuaWdodHMgYW5kIGRhbWVzIG9mIGhpcyBjb3VydCwgYW5kIHdpdGggdGhlc2UgcmV0aXJlZCB0byB0aGUgZGVlcCBzZWNsdXNpb24gb2Ygb25lIG9mIGhpcyBjcmVuZWxsYXRlZCBhYmJleXMuIFRoaXMgd2FzIGFuIGV4dGVuc2l2ZSBhbmQgbWFnbmlmaWNlbnQgc3RydWN0dXJlLCB0aGUgY3JlYXRpb24gb2YgdGhlIHByaW5jZSdzIG93biBlY2NlbnRyaWMgeWV0IGF1Z3VzdCB0YXN0ZS4gQSBzdHJvbmcgYW5kIGxvZnR5IHdhbGwgZ2lyZGxlZCBpdCBpbi4gVGhpcyB3YWxsIGhhZCBnYXRlcyBvZiBpcm9uLiBUaGUgY291cnRpZXJzLCBoYXZpbmcgZW50ZXJlZCwgYnJvdWdodCBmdXJuYWNlcyBhbmQgbWFzc3kgaGFtbWVycyBhbmQgd2VsZGVkIHRoZSBib2x0cy4=",
	}
	adapter.GetMock().On("ReadForUser", "test_user").Return([]map[string]interface{}{
		{
			"_id":          "xid",
			"startdate":    time.Now(),
			"owner":        "test_user",
			"resourcename": resourceName1,
			"cluster":      clusterStringMap,
			"namespace":    "test_user",
		},
		{
			"_id":          "xid",
			"startdate":    time.Now(),
			"owner":        "test_user",
			"resourcename": resourceName2,
			"cluster":      clusterStringMap,
			"namespace":    "test_user",
		},
	}, nil)

	wfds, err := db.GetRunnersForUser("test_user")
	assert.NoError(t, err)
	assert.Len(t, wfds, 2)
}

func testGetRunner(t *testing.T) {
	runnerBson, err := bson.Marshal(testRunner)
	assert.NoError(t, err)
	adapter := common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Read", "xid").Return(runnerBson, nil)

	db = &DB{
		adapter: adapter,
	}

	runner, err := db.GetRunner("xid", "test_user")
	assert.NoError(t, err)
	assert.Equal(t, testStartDate, runner.StartDate)
	assert.Equal(t, "wdfxid", runner.WorkflowDefinitionID)
	assert.Equal(t, "test_user", runner.Owner)
	assert.Equal(t, "test_user-nafigos-say-bqd4sg30ip20n8068ocg-ooooooooooo", runner.ResourceName)
	assert.Equal(t, "wsimpson", runner.Namespace)
	assert.Equal(t, testCluster, runner.Cluster)
}

func testGetRunnerNotAuthorized(t *testing.T) {
	runnerBson, err := bson.Marshal(testRunner)
	assert.NoError(t, err)
	adapter := common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Read", "xid").Return(runnerBson, nil)
	db = &DB{
		adapter: adapter,
	}

	runner, err := db.GetRunner("xid", "not_test_user")
	assert.Error(t, err)
	assert.Empty(t, runner)
}

func TestGetRedacted(t *testing.T) {
	redactedRunner := testRunner.GetRedacted()
	assert.Equal(t, "REDACTED", redactedRunner.Cluster.Config)
	// make sure original Runner is unchanged
	assert.Equal(t, "VGhlIHJlZCBkZWF0aCBoYWQgbG9uZyBkZXZhc3RhdGVkIHRoZSBjb3VudHJ5LiBObyBwZXN0aWxlbmNlIGhhZCBldmVyIGJlZW4gc28gZmF0YWwsIG9yIHNvIGhpZGVvdXMuIEJsb29kIHdhcyBpdHMgQXZhdGFyIGFuZCBpdHMgc2VhbCAtLSB0aGUgbWFkbmVzcyBhbmQgdGhlIGhvcnJvciBvZiBibG9vZC4gVGhlcmUgd2VyZSBzaGFycCBwYWlucywgYW5kIHN1ZGRlbiBkaXp6aW5lc3MsIGFuZCB0aGVuIHByb2Z1c2UgYmxlZWRpbmcgYXQgdGhlIHBvcmVzLCB3aXRoIGRpc3NvbHV0aW9uLiBUaGUgc2NhcmxldCBzdGFpbnMgdXBvbiB0aGUgYm9keSBhbmQgZXNwZWNpYWxseSB1cG9uIHRoZSBmYWNlIG9mIHRoZSB2aWN0aW0sIHdlcmUgdGhlIHBlc3QgYmFuIHdoaWNoIHNodXQgaGltIG91dCBmcm9tIHRoZSBhaWQgYW5kIGZyb20gdGhlIHN5bXBhdGh5IG9mIGhpcyBmZWxsb3ctbWVuLiBBbmQgdGhlIHdob2xlIHNlaXp1cmUsIHByb2dyZXNzLCBhbmQgdGVybWluYXRpb24gb2YgdGhlIGRpc2Vhc2UsIHdlcmUgaW5jaWRlbnRzIG9mIGhhbGYgYW4gaG91ci4KCkJ1dCBQcmluY2UgUHJvc3Blcm8gd2FzIGhhcHB5IGFuZCBkYXVudGxlc3MgYW5kIHNhZ2FjaW91cy4gV2hlbiBoaXMgZG9taW5pb25zIHdlcmUgaGFsZiBkZXBvcHVsYXRlZCwgaGUgc3VtbW9uZWQgdG8gaGlzIHByZXNlbmNlIGEgdGhvdXNhbmQgaGFsZSBhbmQgbGlnaHQtaGVhcnRlZCBmcmllbmRzIGZyb20gYW1vbmcgdGhlIGtuaWdodHMgYW5kIGRhbWVzIG9mIGhpcyBjb3VydCwgYW5kIHdpdGggdGhlc2UgcmV0aXJlZCB0byB0aGUgZGVlcCBzZWNsdXNpb24gb2Ygb25lIG9mIGhpcyBjcmVuZWxsYXRlZCBhYmJleXMuIFRoaXMgd2FzIGFuIGV4dGVuc2l2ZSBhbmQgbWFnbmlmaWNlbnQgc3RydWN0dXJlLCB0aGUgY3JlYXRpb24gb2YgdGhlIHByaW5jZSdzIG93biBlY2NlbnRyaWMgeWV0IGF1Z3VzdCB0YXN0ZS4gQSBzdHJvbmcgYW5kIGxvZnR5IHdhbGwgZ2lyZGxlZCBpdCBpbi4gVGhpcyB3YWxsIGhhZCBnYXRlcyBvZiBpcm9uLiBUaGUgY291cnRpZXJzLCBoYXZpbmcgZW50ZXJlZCwgYnJvdWdodCBmdXJuYWNlcyBhbmQgbWFzc3kgaGFtbWVycyBhbmQgd2VsZGVkIHRoZSBib2x0cy4=", testRunner.Cluster.Config)
}
