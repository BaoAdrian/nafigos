package test

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

var adminToken, userToken string

func newRequest(verb, uri, body, token string) (*http.Request, error) {
	req, err := http.NewRequest(verb, uri, nil)
	if err != nil {
		return req, err
	}
	req.Header.Add("Authorization", token)
	if len(body) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader([]byte(body)))
	}
	return req, nil
}
