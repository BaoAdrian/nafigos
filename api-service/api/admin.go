package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
)

var (
	userClusterHost = "ca-run.cyverse.org"
)

// AdminAPIRouter routes all the functions to admin API endpoints
func AdminAPIRouter(router *mux.Router) {
	if len(os.Getenv("USER_CLUSTER_HOST")) > 0 {
		userClusterHost = os.Getenv("USER_CLUSTER_HOST")
	}
	adminRouter := router.NewRoute().Subrouter()
	adminRouter.Use(adminMiddleware)
	adminRouter.HandleFunc("/users", createUser).Methods("POST")
	adminRouter.HandleFunc("/users/{username}", deleteUser).Methods("DELETE")
}

// adminMiddleware is used to enforce admin permissions on these endpoints
func adminMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := log.WithFields(log.Fields{
			"package":  "api",
			"function": "adminMiddleware",
		})

		// If using userpass, no admin access enforcement
		if vaultClient.GetAuthMethod() != "keycloak" {
			next.ServeHTTP(w, r)
			return
		}

		// Validate with Keycloak
		accessToken := r.Header.Get("Authorization")
		idToken, err := keycloakClient.Verify(accessToken)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to verify access token")
			jsonError(w, err.Error(), http.StatusForbidden)
			return
		}

		// Check if user is admin from idToken claims
		idTokenClaims := struct {
			Username     string `json:"preferred_username"`
			NafigosAdmin string `json:"nafigos_admin"`
		}{}
		err = idToken.Claims(&idTokenClaims)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to read ID token claims")
			jsonError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// error and exit if user is not admin
		if idTokenClaims.NafigosAdmin != "[nafigos_admin]" {
			logger.WithFields(log.Fields{"user": idTokenClaims.Username}).Warn("non-admin user attempted to access admin-only endpoint")
			jsonError(w, "this action is only available for admins", http.StatusForbidden)
			return
		}
		r.Header.Add("X-Nafigos-User", idTokenClaims.Username)

		logger.WithFields(log.Fields{"user": idTokenClaims.Username}).Info("admin user verified")
		next.ServeHTTP(w, r)
	})
}

func createUser(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createUser",
	})

	accessToken := r.Header.Get("Authorization")

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		jsonError(w, "unable to read request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	var user struct {
		CreateKubeconfig bool `json:"create_kubeconfig"`
		common.User
	}
	err = json.Unmarshal(reqBody, &user)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse request body")
		jsonError(w, "unable to parse request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	logger = logger.WithFields(log.Fields{"username": user.Username})

	slug, err := common.GetDNS1123Name(user.Username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create cluster slug for user")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// If using userpass auth, add user to tokenStore
	if vaultClient.GetAuthMethod() != "keycloak" {
		if len(user.Username) == 0 || len(user.Password) == 0 {
			logger.WithFields(log.Fields{"error": err}).Error("not enough information to create user")
			jsonError(w, "not enough information to create user", http.StatusBadRequest)
			return
		}
		tokenStore[user.Password] = user.Username
	}
	// Mount new path that is accessible by the policy
	code, err := vaultClient.MountNewKVPath(user.Username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to mount new KV path in Vault")
		jsonError(w, "unable to mount new KV path in Vault: "+err.Error(), code)
		return
	}
	// Create a policy for the user and JWT role to access it
	code, err = vaultClient.CreateNewPolicy(user.Username, user.IsAdmin)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create Vault Policy")
		jsonError(w, "unable to create Vault Policy: "+err.Error(), code)
		return
	}
	if vaultClient.GetAuthMethod() != "keycloak" {
		code, err = vaultClient.WriteUserpass(user.Username, user.Password)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create Userpass")
			jsonError(w, "unable to create Userpass: "+err.Error(), code)
			return
		}
	} else {
		code, err = vaultClient.WriteJWTRole(user.Username, user.IsAdmin)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create JWT role")
			jsonError(w, "unable to create JWT role: "+err.Error(), code)
			return
		}
	}
	// Get a token in order to create dockerhub and github secrets
	vaultToken, err := vaultClient.GetUserLoginToken(user.Username, accessToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to login")
		jsonError(w, "unable to login: "+err.Error(), http.StatusInternalServerError)
		return
	}
	// Write user info to Vault
	code, err = vaultClient.WriteUser(user.User, vaultToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to write user info to Vault:")
		jsonError(w, "unable to write user info to Vault: "+err.Error(), code)
		return
	}
	// Create New user cluster config asynchronously
	if user.CreateKubeconfig {
		clusterID := xid.New().String()
		logger = logger.WithFields(log.Fields{"cluster_id": clusterID})
		logger.Info("creating user cluster config asynchronously")
		go func() {
			user.User.Clusters = map[string]common.Cluster{
				user.Username: {
					ID:    clusterID,
					Name:  "cyverse-user-cluster",
					Host:  userClusterHost,
					Slug:  slug,
					Admin: false,
				},
			}
			msg, err := common.PublishRequest(
				&common.BasicRequest{User: user.User},
				"Pleo.CreateUserClusterKubeconfig",
				"API",
				natsInfo["address"],
			)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to request new config from Pleo")
				return
			}
			var info struct {
				Error   common.Error   `json:"error,omitempty"`
				Cluster common.Cluster `json:"cluster"`
			}
			err = json.Unmarshal(msg, &info)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal clusterconfig json")
				return
			}
			if len(info.Error.Message) > 0 {
				logger.WithFields(log.Fields{"error": fmt.Errorf(info.Error.Message)}).Error("unable to create new kubeconfig")
				return
			}
			_, err = vaultClient.AddCluster(user.Username, info.Cluster.ID, info.Cluster, vaultToken)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to add cluster to Vault")
				return
			}
		}()
	}
	w.WriteHeader(http.StatusCreated)
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteUser",
		"username": username,
	})

	code, err := vaultClient.DeleteUser(username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete user")
		jsonError(w, "unable to delete user: "+err.Error(), code)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
