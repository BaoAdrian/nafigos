package api

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cyverse/nafigos/common"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/nafigos/workflow-definition-service/workflowdefinition"
)

// AnchoreAPIRouter creates routes for user-related operations such as creating and
func AnchoreAPIRouter(router *mux.Router) {
	router.HandleFunc("/images", getReports).Methods("GET")
	router.HandleFunc("/images/{workflowID}", getReports).Methods("GET")
}

func getReports(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "getReports",
		"workflow_id": workflowID,
	})
	logger.Info("received request to get vulnerability reports for user")

	var request workflowdefinition.Request

	/// FIX ME (Need to check if we need all this, for now we will include it)
	status, err := prepareRequest(&request, r, workflowID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")
	/// FIX ME (Need to check if we need all this, for now we will include it)

	msg, err := common.PublishRequest(
		&request,
		"Anchore.Get",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to get WorkflowDefinition")
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(getResponseCode(msg))
	w.Write(msg)
}
