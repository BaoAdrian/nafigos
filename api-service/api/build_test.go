package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"sync"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func testStartBuild(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.DockerhubSecret,
			Username: "test_dockerhubusername",
			Value:    "test_dockerhubtoken",
		}, 200, nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(2)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a217p6i", request.WorkflowID)
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.StreamingTestSubscriber(
		"Build.Build",
		"Build.BuildQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User             common.User `json:"user"`
				RegistrySecretID string      `json:"registry_secret_id"`
				ClusterID        string      `json:"cluster_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.RegistrySecretID)
			assert.NotEmpty(t, request.User.Secrets)
			assert.Equal(t, common.Secret{
				ID:       "9m4e2mr0ui3e8a215n4g",
				Type:     common.DockerhubSecret,
				Username: "test_dockerhubusername",
				Value:    "test_dockerhubtoken",
			}, request.User.Secrets["9m4e2mr0ui3e8a215n4g"])
			assert.Equal(t, "9m4e2mr0ui3e8a216o5h", request.ClusterID)
			assert.Equal(t, common.Cluster{
				ID:               "9m4e2mr0ui3e8a216o5h",
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "config",
			}, request.User.Clusters["9m4e2mr0ui3e8a216o5h"])
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "build",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"registry_secret_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Regexp(t, regexp.MustCompile(`{"id": "[a-z0-9]{20}"}`), rr.Body.String())
}

func testStartBuildRunAfterBuilding(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.DockerhubSecret,
			Username: "test_dockerhubusername",
			Value:    "test_dockerhubtoken",
		}, 200, nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(2)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a217p6i", request.WorkflowID)
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.StreamingTestSubscriber(
		"Build.Build",
		"Build.BuildQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User             common.User `json:"user"`
				RegistrySecretID string      `json:"registry_secret_id"`
				ClusterID        string      `json:"cluster_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.RegistrySecretID)
			assert.NotEmpty(t, request.User.Secrets)
			assert.Equal(t, common.Secret{
				ID:       "9m4e2mr0ui3e8a215n4g",
				Type:     common.DockerhubSecret,
				Username: "test_dockerhubusername",
				Value:    "test_dockerhubtoken",
			}, request.User.Secrets["9m4e2mr0ui3e8a215n4g"])
			assert.Equal(t, "9m4e2mr0ui3e8a216o5h", request.ClusterID)
			assert.Equal(t, common.Cluster{
				ID:               "9m4e2mr0ui3e8a216o5h",
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "config",
			}, request.User.Clusters["9m4e2mr0ui3e8a216o5h"])
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "build",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"registry_secret_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h",
        "run": true
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Regexp(t, regexp.MustCompile(`{"id": "[a-z0-9]{20}", "runID": "[a-z0-9]{20}"}`), rr.Body.String())
}

func testStartBuildBadRequest(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testStartBuildNoClusterID(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "required cluster ID was not provided"}}`, rr.Body.String())
}

func testStartBuildNoSecretID(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "required registry secret ID was not provided"}}`, rr.Body.String())
}

func testStartBuildNoDockerhubSecret(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.GitSecret,
			Username: "test_githubusername",
			Value:    "test_githubtoken",
		}, 200, nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "registry secret is required to push a built container image"}}`, rr.Body.String())
}

func testStartBuildNATSError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.DockerhubSecret,
			Username: "test_dockerhubusername",
			Value:    "test_dockerhubtoken",
		}, 200, nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"registry_secret_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusServiceUnavailable, rr.Code)
	assert.Equal(t, `{"error": {"code": 503, "message": "unable to get WorkflowDefinition data: error connecting to NATS: nats: no servers available for connection"}}`, rr.Body.String())
}

func testStartBuildGetWorkflowError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.DockerhubSecret,
			Username: "test_dockerhubusername",
			Value:    "test_dockerhubtoken",
		}, 200, nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a217p6i", request.WorkflowID)
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"registry_secret_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to get WorkflowDefinition data: mock error"}}`, rr.Body.String())
}

func testStartBuildErrorReadingCluster(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.DockerhubSecret,
			Username: "test_dockerhubusername",
			Value:    "test_dockerhubtoken",
		}, 200, nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"registry_secret_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 403, "message": "Error reading cluster '9m4e2mr0ui3e8a216o5h': permission denied"}}`, rr.Body.String())
}

func testStartBuildBadWorkflow(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.DockerhubSecret,
			Username: "test_dockerhubusername",
			Value:    "test_dockerhubtoken",
		}, 200, nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a216o5h", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a216o5h",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				WorkflowID string `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a217p6i", request.WorkflowID)
			msg.Respond([]byte(`{"data": {"error_msg": "Error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("POST", "/workflows/9m4e2mr0ui3e8a217p6i/builds", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"registry_secret_id": "9m4e2mr0ui3e8a215n4g",
	"cluster_id": "9m4e2mr0ui3e8a216o5h"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Unable to build WorkflowDefinition in an error state. Please fix Workflow and try again"}}`, rr.Body.String())
}

func testGetBuild(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetBuild",
		"Phylax.GetBuildQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User    common.User `json:"user"`
				BuildID string      `json:"build_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "build_id", request.BuildID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"build": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/builds/build_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/builds/{buildID}", getBuild)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"build": {}}`, rr.Body.String())
}

func testGetBuildBadRequest(t *testing.T) {
	// Create request
	req, err := http.NewRequest("GET", "/builds/build_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/builds/{buildID}", getBuild)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testGetBuildError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetBuild",
		"Phylax.GetBuildQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User    common.User `json:"user"`
				BuildID string      `json:"build_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "build_id", request.BuildID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/builds/build_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/builds/{buildID}", getBuild)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "mock error"}}`, rr.Body.String())
}

func testGetBuildNATSError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("GET", "/builds/build_id", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/builds/{buildID}", getBuild)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to publish request to get builds: error connecting to NATS: nats: no servers available for connection"}}`, rr.Body.String())
}
