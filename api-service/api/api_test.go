package api

import (
	"io/ioutil"
	"testing"

	"github.com/nats-io/nats-streaming-server/server"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

const (
	rootToken    = "foo"
	vaultAddress = "http://127.0.0.1:8200"
)

func init() {
	natsInfo = map[string]string{
		"cluster_id": "nafigos-cluster",
		"client_id":  "api",
		"address":    "nats://localhost:4222",
	}
	log.SetOutput(ioutil.Discard)
}

func runNATSServer(t *testing.T) *server.StanServer {
	s, err := server.RunServer("nafigos-cluster")
	assert.NoError(t, err)
	return s
}

type testMap map[string]func(t *testing.T)

func TestAdminAPI(t *testing.T) {
	tests := testMap{
		"CreateUser":                      testCreateUser,
		"CreateUserBadRequest":            testCreateUserBadRequest,
		"CreateUserWithNewKubeconfig":     testCreateUserWithNewKubeconfig,
		"CreateUserNotEnoughInfoError":    testCreateUserNotEnoughInfoError,
		"CreateUserPartialInfoNoError":    testCreateUserPartialInfoNoError,
		"CreateUserErrorCreatingPolicy":   testCreateUserErrorCreatingPolicy,
		"CreateUserErrorCreatingUserpass": testCreateUserErrorCreatingUserpass,
		"CreateUserErrorGettingToken":     testCreateUserErrorGettingToken,
		"CreateUserErrorWritingUser":      testCreateUserErrorWritingUser,
		"CreateUserDuplicate":             testCreateUserDuplicate,
		"DeleteUser":                      testDeleteUser,
		"DeleteUserError":                 testDeleteUserError,
	}
	for name, test := range tests {
		t.Run(name, test)
	}
}

func TestUserAPI(t *testing.T) {
	tests := testMap{
		"UpdateUser":                    testUpdateUser,
		"UpdateUserAuthError":           testUpdateUserAuthError,
		"UpdateUserBadRequest":          testUpdateUserBadRequest,
		"UpdateUserPermissionDenied":    testUpdateUserPermissionDenied,
		"GetUser":                       testGetUser,
		"GetUserAuthError":              testGetUserAuthError,
		"GetUserPermissionDenied":       testGetUserPermissionDenied,
		"GetUsers":                      testGetUsers,
		"GetUsersError":                 testGetUsersError,
		"GetUsersAuthError":             testGetUsersAuthError,
		"GetAllSecrets":                 testGetAllSecrets,
		"GetAllSecretsAuthError":        testGetAllSecretsAuthError,
		"GetAllSecretsPermissionDenied": testGetAllSecretsPermissionDenied,
		"AddNewSecret":                  testAddNewSecret,
		"AddNewSecretAuthError":         testAddNewSecretAuthError,
		"AddNewSecretBadRequest":        testAddNewSecretBadRequest,
		"AddNewSecretPermissionDenied":  testAddNewSecretPermissionDenied,
		"GetSecret":                     testGetSecret,
		"GetSecretAuthError":            testGetSecretAuthError,
		"GetSecretPermissionDenied":     testGetSecretPermissionDenied,
		"EditSecret":                    testEditSecret,
		"EditSecretAuthError":           testEditSecretAuthError,
		"EditSecretBadRequest":          testEditSecretBadRequest,
		"EditSecretPermissionDenied":    testEditSecretPermissionDenied,
		"DeleteSecret":                  testDeleteSecret,
		"DeleteSecretAuthError":         testDeleteSecretAuthError,
		"DeleteSecretPermissionDenied":  testDeleteSecretPermissionDenied,
	}
	for name, test := range tests {
		t.Run(name, test)
	}
}

func TestClustersAPI(t *testing.T) {
	tests := testMap{
		"GetAllClusters":                       testGetAllClusters,
		"GetAllClustersAuthError":              testGetAllClustersAuthError,
		"GetAllClustersPermissionDenied":       testGetAllClustersPermissionDenied,
		"AddNewCluster":                        testAddNewCluster,
		"AddNewClusterAuthError":               testAddNewClusterAuthError,
		"AddNewClusterBadRequest":              testAddNewClusterBadRequest,
		"AddNewClusterPermissionDenied":        testAddNewClusterPermissionDenied,
		"AddNewClusterNameMissing":             testAddNewClusterNameMissing,
		"AddNewClusterDefaultNamespaceMissing": testAddNewClusterDefaultNamespaceMissing,
		"GetCluster":                           testGetCluster,
		"GetClusterAuthError":                  testGetClusterAuthError,
		"GetClusterPermissionDenied":           testGetClusterPermissionDenied,
		"UpdateCluster":                        testUpdateCluster,
		"UpdateClusterAuthError":               testUpdateClusterAuthError,
		"UpdateClusterBadRequest":              testUpdateClusterBadRequest,
		"UpdateClusterPermissionDenied":        testUpdateClusterPermissionDenied,
		"DeleteCluster":                        testDeleteCluster,
		"DeleteClusterAuthError":               testDeleteClusterAuthError,
		"DeleteClusterPermissionDenied":        testDeleteClusterPermissionDenied,
		"GetClusterResources":                  testGetClusterResources,
		"GetResourcesAuthError":                testGetResourcesAuthError,
		"GetResourcesPermissionDenied":         testGetResourcesPermissionDenied,
		"GetResourcesNATSError":                testGetResourcesNATSError,
		"GetResourcesInvalidNamespace":         testGetResourcesInvalidNamespace,
		"GetResourcesBadNATSResponse":          testGetResourcesBadNATSResponse,
	}
	for name, test := range tests {
		t.Run(name, test)
	}
}

func TestWorkflowAPI(t *testing.T) {
	tests := testMap{
		"GetWorkflowDefinitionsForUser":           testGetWorkflowDefinitionsForUser,
		"GetWorkflowDefinitionsForUserBadRequest": testGetWorkflowDefinitionsForUserBadRequest,
		"GetWorkflowDefinitionsForUserError":      testGetWorkflowDefinitionsForUserError,
		"GetWorkflowDefinitionsForUserNATSError":  testGetWorkflowDefinitionsForUserNATSError,
		"CreateWorkflowDefinition":                testCreateWorkflowDefinition,
		"CreateWorkflowDefinitionNoData":          testCreateWorkflowDefinitionNoData,
		"CreateWorkflowDefinitionBadData":         testCreateWorkflowDefinitionBadData,
		"CreateWorkflowDefinitionWithSecret":      testCreateWorkflowDefinitionWithSecret,
		"CreateWorkflowDefinitionWithSecretError": testCreateWorkflowDefinitionWithSecretError,
		"GetWorkflowDefinition":                   testGetWorkflowDefinition,
		"GetWorkflowDefinitionBadRequest":         testGetWorkflowDefinitionBadRequest,
		"GetWorkflowDefinitionNATSError":          testGetWorkflowDefinitionNATSError,
		"GetWorkflowDefinitionError":              testGetWorkflowDefinitionError,
		"DeleteWorkflowDefinition":                testDeleteWorkflowDefinition,
		"DeleteWorkflowDefinitionError":           testDeleteWorkflowDefinitionError,
		"UpdateWorkflowDefinition":                testUpdateWorkflowDefinition,
		"UpdateWorkflowDefinitionNoSecret":        testUpdateWorkflowDefinitionNoSecret,
		"UpdateWorkflowDefinitionError":           testUpdateWorkflowDefinitionError,
		"UpdateWorkflowDefinitionNoData":          testUpdateWorkflowDefinitionNoData,
		"UpdateWorkflowDefinitionBadData":         testUpdateWorkflowDefinitionBadData,
	}
	for name, test := range tests {
		t.Run(name, test)
	}
}

func TestBuildAPI(t *testing.T) {
	tests := testMap{
		"StartBuild":                    testStartBuild,
		"StartBuildRunAfterBuild":       testStartBuildRunAfterBuilding,
		"StartBuildBadRequest":          testStartBuildBadRequest,
		"StartBuildNoClusterID":         testStartBuildNoClusterID,
		"StartBuildNoSecretID":          testStartBuildNoSecretID,
		"StartBuildNATSError":           testStartBuildNATSError,
		"StartBuildGetWorkflowError":    testStartBuildGetWorkflowError,
		"StartBuildErrorReadingCluster": testStartBuildErrorReadingCluster,
		"StartBuildBadWorkflow":         testStartBuildBadWorkflow,
		"GetBuild":                      testGetBuild,
		"GetBuildBadRequest":            testGetBuildBadRequest,
		"GetBuildError":                 testGetBuildError,
		"GetBuildNATSError":             testGetBuildNATSError,
	}
	for name, test := range tests {
		t.Run(name, test)
	}
}

func TestRunAPI(t *testing.T) {
	tests := testMap{
		"StartRun":                 testStartRun,
		"StartRunExtraFlags":       testStartRunExtraFlags,
		"StartRunBadRequest":       testStartRunBadRequest,
		"StartRunNoClusterID":      testStartRunNoClusterID,
		"StartRunNoWorkflowID":     testStartRunNoWorkflowID,
		"StartRunGetWorkflowError": testStartRunGetWorkflowError,
		"StartRunBadWorkflow":      testStartRunBadWorkflow,
		"GetRun":                   testGetRun,
		"GetRunBadRequest":         testGetRunBadRequest,
		"GetRunError":              testGetRunError,
		"GetRunNATSError":          testGetRunNATSError,
		"GetRunsForUser":           testGetRunsForUser,
		"GetRunsForUserBadRequest": testGetRunsForUserBadRequest,
		"GetRunsForUserError":      testGetRunsForUserError,
		"GetRunsForUserNATSError":  testGetRunsForUserNATSError,
		"UpdateRun":                testUpdateRun,
		"DeleteRun":                testDeleteRun,
		"DeleteRunBadRequest":      testDeleteRunBadRequest,
		"ScaleFromZero":            testScaleFromZero,
		"ScaleFromZeroBadRequest":  testScaleFromZeroBadRequest,
	}
	for name, test := range tests {
		t.Run(name, test)
	}
}
