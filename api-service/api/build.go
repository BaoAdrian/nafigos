package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/cyverse/nafigos/build-service/build"
	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/phylax-service/phylax"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
)

// BuildAPIRouter creates routes for interacting with the Build Service
func BuildAPIRouter(router *mux.Router) {
	router.HandleFunc("/workflows/{workflowID}/builds", buildWorkflowDefinition).Methods("POST")
	router.HandleFunc("/builds/{buildID}", getBuild).Methods("GET")
	router.HandleFunc("/builds", getBuildsForUser).Methods("GET")
}

func buildWorkflowDefinition(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	buildID := xid.New().String()
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "buildWorkflowDefinition",
		"workflow_id": workflowID,
		"build_id":    buildID,
	})
	logger.Info("received request to build images")

	var request build.Request
	status, err := prepareRequest(&request, r, workflowID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Check that necessary details were included in the request
	if len(request.GetClusterID()) == 0 {
		err = fmt.Errorf("required cluster ID was not provided")
		logger.WithFields(log.Fields{"error": err}).Error("unable to build Workflow without cluster ID")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}
	if len(request.GetRegistrySecretID()) == 0 {
		err = fmt.Errorf("required registry secret ID was not provided")
		logger.WithFields(log.Fields{"error": err}).Error("unable to build Workflow without registry secret ID")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Now get WorkflowDefinition data
	status, err = getWorkflowDefinitionData(&request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition data")
		jsonError(w, "unable to get WorkflowDefinition data: "+err.Error(), status)
		return
	}

	// Make sure WorkflowDefinition error is not in an Error state before continuing
	if len(request.WorkflowDefinition.Error) > 0 {
		log.Println("Unable to build WorkflowDefinition in error state")
		jsonError(w, "Unable to build WorkflowDefinition in an error state. Please fix Workflow and try again", http.StatusBadRequest)
		return
	}

	// Generate XID for this Build execution
	request.SetBuildID(buildID)
	if request.RunAfterBuild {
		request.SetRunID(xid.New().String())
	}

	logRequest = request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	// Now submit Build request
	reqBody, err := json.Marshal(request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request JSON")
		jsonError(w, "unable to marshal request JSON: "+err.Error(), http.StatusInternalServerError)
		return
	}
	err = common.StreamingPublish(reqBody, natsInfo, "Build.Build", "API")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish Build request")
		jsonError(w, "unable to publish Build request: "+err.Error(), http.StatusInternalServerError)
		return
	}

	if request.RunAfterBuild {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(fmt.Sprintf(`{"id": "%s", "runID": "%s"}`, request.GetBuildID(), request.GetRunID())))
	} else {
		idResponse(w, request.GetBuildID(), http.StatusAccepted)
	}
}

func getBuild(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	buildID := mux.Vars(r)["buildID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "getBuild",
		"workflow_id": workflowID,
		"build_id":    buildID,
	})
	logger.Info("received request to get build")

	var request build.Request
	status, err := prepareRequest(&request, r, mux.Vars(r)["buildID"])
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	request.SetBuildID(mux.Vars(r)["buildID"])

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Get info from Phylax
	msg, err := common.PublishRequest(
		&request,
		"Phylax.GetBuild",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to get builds")
		jsonError(w, "unable to publish request to get builds: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(getResponseCode(msg))
	w.Write(msg)
}

func getBuildsForUser(w http.ResponseWriter, r *http.Request) {
	workflowID := mux.Vars(r)["workflowID"]
	buildID := mux.Vars(r)["buildID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "getBuildsForUser",
		"workflow_id": workflowID,
		"build_id":    buildID,
	})
	var request phylax.Request
	status, err := prepareRequest(&request, r, "")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	request.SetBuildID(buildID)

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Get info from Phylax
	msg, err := common.PublishRequest(
		&request,
		"Phylax.GetBuildsForUser",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to get build")
		jsonError(w, "unable to publish request to get build: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(getResponseCode(msg))
	w.Write(msg)
}
