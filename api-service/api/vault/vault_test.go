package vault

import (
	"testing"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/hashicorp/vault/command"
	"github.com/stretchr/testify/assert"
)

const (
	rootToken    = "foo"
	vaultAddress = "http://127.0.0.1:8200"
)

var testClient *Client

func init() {
	go testServer()
	var err error
	testClient, err = NewClient(vaultAddress, rootToken, "")
	if err != nil {
		panic(err)
	}
}

func testServer() {
	command.Run([]string{
		"server",
		"-dev",
		"-dev-root-token-id=" + rootToken,
	})
}

func TestReadVaultError(t *testing.T) {
	code, err := readVaultError(`URL: GET http://127.0.0.1:8200/v1/nafigos/testuser/secrets/github
Code: 403. Errors:

* 1 error occurred:
	* permission denied
`)
	assert.Error(t, err)
	assert.Equal(t, 403, code)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
}

func TestReadVaultErrorNoCode(t *testing.T) {
	code, err := readVaultError("ERROR WOW")
	assert.Error(t, err)
	assert.Equal(t, 0, code)
	assert.Equal(t, "unable to find code in error message", err.Error())
}

func TestReadVaultErrorNoMessage(t *testing.T) {
	code, err := readVaultError(`URL: GET http://127.0.0.1:8200/v1/nafigos/testuser/secrets/github
Code: 403. Errors:
`)
	assert.Error(t, err)
	assert.Equal(t, 403, code)
	assert.Equal(t, "unable to find specific message in error", err.Error())
}

func TestNewClient(t *testing.T) {
	client, err := NewClient(vaultAddress, rootToken, "")
	assert.NoError(t, err)
	assert.Equal(t, rootToken, client.Token())
}

func TestGetAuthMethod(t *testing.T) {
	client, err := NewClient(vaultAddress, rootToken, "")
	assert.NoError(t, err)
	assert.Equal(t, "userpass", client.GetAuthMethod())
}

func TestEnableUserPass(t *testing.T) {
	err := testClient.EnableUserpassAuth()
	assert.NoError(t, err)
}

func TestEnableUserPassError(t *testing.T) {
	err := testClient.EnableUserpassAuth()
	assert.Error(t, err)
	assert.Equal(t, "Error making API request.\n\nURL: POST http://127.0.0.1:8200/v1/sys/auth/userpass\nCode: 400. Errors:\n\n* path is already in use at userpass/", err.Error())
}

func TestMountNewKVPath(t *testing.T) {
	code, err := testClient.MountNewKVPath("testuser")
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestMountDuplicateKVPathFail(t *testing.T) {
	code, err := testClient.MountNewKVPath("testuser")
	assert.Error(t, err)
	assert.Equal(t, 400, code)
	assert.Equal(t, "path is already in use at nafigos/testuser/", err.Error())
}

func TestCreateNewPolicy(t *testing.T) {
	code, err := testClient.CreateNewPolicy("testuser", false)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestCreateNewAdmin(t *testing.T) {
	code, err := testClient.CreateNewPolicy("adminuser", true)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestCreateAdminPolicy(t *testing.T) {
	code, err := testClient.CreateAdminPolicy()
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestWriteUserpass(t *testing.T) {
	code, err := testClient.WriteUserpass("testuser", "testpassword")
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestWriteUserpassError(t *testing.T) {
	code, err := testClient.WriteUserpass("", "")
	assert.Error(t, err)
	assert.Equal(t, "1 error occurred:\nunsupported operation", err.Error())
	assert.Equal(t, 405, code)
}

func TestGetUserLoginToken(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
}

func TestGetUserLoginTokenFail(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "badpassword")
	assert.Error(t, err)
	assert.Equal(t, "no token returned from login", err.Error())
	assert.Empty(t, token)
}

func TestWriteUser(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	user := common.User{
		Username: "testuser",
		Secrets: map[string]common.Secret{
			"1": {
				Username: "github_user",
				Value:    "github_token",
				Type:     common.GitSecret,
			},
			"2": {
				Username: "dockerhub_user",
				Value:    "dockerhub_token",
				Type:     common.DockerhubSecret,
			},
		},
		Clusters: map[string]common.Cluster{
			"1": {
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "test",
			},
		},
	}
	code, err := testClient.WriteUser(user, token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestWriteUserError(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	user := common.User{
		Username: "testuser2",
		Secrets: map[string]common.Secret{
			"1": {
				Username: "github_user",
				Value:    "github_token",
				Type:     common.GitSecret,
			},
			"2": {
				Username: "dockerhub_user",
				Value:    "dockerhub_token",
				Type:     common.DockerhubSecret,
			},
		},
	}
	code, err := testClient.WriteUser(user, token)
	assert.Error(t, err)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
	assert.Equal(t, 403, code)
}

func TestAddSecret(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	code, err := testClient.AddSecret("testuser", "ID",
		map[string]interface{}{
			"username": "github_user",
			"value":    "github_token",
			"type":     common.GitSecret,
		}, token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestAddSecretUnauthorized(t *testing.T) {
	// First create a new user
	testClient.CreateNewPolicy("testuser2", false)
	testClient.WriteUserpass("testuser2", "testpassword2")
	// Now login as new user
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	// Finally, perform unauthorized action
	code, err := testClient.AddSecret("testuser", "ID",
		map[string]interface{}{
			"username": "github_user",
			"value":    "github_token",
			"type":     common.GitSecret,
		}, token)
	assert.Error(t, err)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
	assert.Equal(t, 403, code)
}

func TestReadSecret(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	secret, code, err := testClient.ReadSecret("testuser", "ID", token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
	assert.Equal(t, "github_user", secret.Username)
	assert.Equal(t, "github_token", secret.Value)
}

func TestAddCluster(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	code, err := testClient.AddCluster("testuser", "ID",
		common.Cluster{
			Name:             "default2",
			DefaultNamespace: "default2",
			Config:           "test2",
			ID:               "ID",
			Host:             "host",
			Slug:             "default2",
			Admin:            false,
		}, token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestReadCluster(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	cluster, code, err := testClient.ReadCluster("testuser", "ID", token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
	assert.Equal(t, "default2", cluster.Name)
	assert.Equal(t, "default2", cluster.DefaultNamespace)
	assert.Equal(t, "test2", cluster.Config)
}

func TestReadClusterError(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	cluster, code, err := testClient.ReadCluster("testuser2", "ID", token)
	assert.Error(t, err)
	assert.Equal(t, 403, code)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
	assert.Empty(t, cluster)
}

func TestEditCluster(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)

	code, err := testClient.EditCluster("testuser", "ID", common.Cluster{Name: "NewName"}, token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)

	cluster, _, _ := testClient.ReadCluster("testuser", "ID", token)
	assert.Equal(t, "NewName", cluster.Name)
	assert.Equal(t, "default2", cluster.DefaultNamespace)
	assert.Equal(t, "test2", cluster.Config)
}

func TestEditClusterPermissionDenied(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)

	code, err := testClient.EditCluster("testuser", "ID", common.Cluster{Name: "NewName"}, token)
	assert.Error(t, err)
	assert.Equal(t, 403, code)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
}

func TestReadUser(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	user, code, err := testClient.ReadUser("testuser", token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
	assert.NotEmpty(t, user)
	assert.Equal(t, common.Secret{
		Username: "github_user",
		Value:    "github_token",
		Type:     common.GitSecret,
		ID:       "ID",
	}, user.Secrets["ID"])
	assert.Equal(t, common.Cluster{
		Name:             "NewName",
		DefaultNamespace: "default2",
		Config:           "test2",
		ID:               "ID",
		Host:             "host",
		Slug:             "default2",
		Admin:            false,
	}, user.Clusters["ID"])
}

func TestReadUserDNE(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	user, code, err := testClient.ReadUser("fake_user", token)
	assert.Error(t, err)
	assert.Equal(t, 404, code)
	assert.Empty(t, user)
	assert.Equal(t, "user 'fake_user' not found", err.Error())
}

func TestReadSecretUnauthorized(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	secret, code, err := testClient.ReadSecret("testuser", "FAKE_ID", token)
	assert.Error(t, err)
	assert.Equal(t, 403, code)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
	assert.Empty(t, secret)
}

func TestEditSecret(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)

	code, err := testClient.EditSecret("testuser", "ID", common.Secret{Type: common.GCRSecret}, token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)

	secret, _, _ := testClient.ReadSecret("testuser", "ID", token)
	assert.Equal(t, common.GCRSecret, secret.Type)
	assert.Equal(t, "github_user", secret.Username)
	assert.Equal(t, "github_token", secret.Value)
}

func TestEditSecretPermissionDenied(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)

	code, err := testClient.EditSecret("testuser", "ID", common.Secret{Type: "registry"}, token)
	assert.Error(t, err)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
	assert.Equal(t, 403, code)
}

func TestReadAllSecrets(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	secrets, code, err := testClient.GetAllSecrets("testuser", token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
	assert.NotEmpty(t, secrets)
	assert.Equal(t, common.Secret{
		Username: "github_user",
		Value:    "github_token",
		Type:     common.GCRSecret,
		ID:       "ID",
	}, secrets["ID"])
}

func TestReadAllSecretsNoSecrets(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	secrets, code, err := testClient.GetAllSecrets("testuser2", token)
	assert.Error(t, err)
	assert.Equal(t, "No secrets found for user 'testuser2'", err.Error())
	assert.Equal(t, 404, code)
	assert.Empty(t, secrets)
}

func TestReadAllClusters(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	clusters, code, err := testClient.GetAllClusters("testuser", token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
	assert.NotEmpty(t, clusters)
	assert.Equal(t, common.Cluster{
		Name:             "NewName",
		DefaultNamespace: "default2",
		Config:           "test2",
		ID:               "ID",
		Host:             "host",
		Slug:             "default2",
		Admin:            false,
	}, clusters["ID"])
}

func TestReadAllClustersNoClusters(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	clusters, code, err := testClient.GetAllClusters("testuser2", token)
	assert.Error(t, err)
	assert.Equal(t, "No clusters found for user 'testuser2'", err.Error())
	assert.Equal(t, 404, code)
	assert.Empty(t, clusters)
}

func TestDeleteSecret(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	code, err := testClient.DeleteSecret("testuser", "ID", token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestDeleteSecretError(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	code, err := testClient.DeleteSecret("testuser", "ID", token)
	assert.Error(t, err)
	assert.Equal(t, 403, code)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
}

func TestListAllUsers(t *testing.T) {
	users, code, err := testClient.ListAllUsers()
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
	assert.NotEmpty(t, users)
	assert.Len(t, users, 1)
	assert.Contains(t, users, "testuser")
}

func TestDeleteCluster(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser", "testpassword")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	code, err := testClient.DeleteCluster("testuser", "ID", token)
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}

func TestDeleteClusterError(t *testing.T) {
	token, err := testClient.GetUserLoginToken("testuser2", "testpassword2")
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	code, err := testClient.DeleteCluster("testuser", "ID", token)
	assert.Error(t, err)
	assert.Equal(t, 403, code)
	assert.Equal(t, "1 error occurred:\npermission denied", err.Error())
}

func TestDeleteUser(t *testing.T) {
	code, err := testClient.DeleteUser("testuser")
	assert.NoError(t, err)
	assert.Equal(t, 200, code)
}
