package vault

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/cyverse/nafigos/common"
)

func clustersPath(owner string) string {
	return namespace + owner + "/clusters/"
}

// AddCluster is used to add a cluster to the user's path in Vault
func (client *Client) AddCluster(owner string, id string, cluster common.Cluster, token string) (int, error) {
	clusterMap := map[string]interface{}{
		"name":              cluster.Name,
		"default_namespace": cluster.DefaultNamespace,
		"config":            cluster.Config,
		"host":              cluster.Host,
		"slug":              cluster.Slug,
		"admin":             cluster.Admin,
	}
	return client.write(clustersPath(owner), id, clusterMap, token)
}

// ReadCluster reads cluster from Vault
func (client *Client) ReadCluster(owner, id, token string) (common.Cluster, int, error) {
	userClient, err := client.getUserClient(token)
	if err != nil {
		code, err := readVaultError(err.Error())
		return common.Cluster{}, code, err
	}
	cluster, err := userClient.Logical().Read(clustersPath(owner) + id)
	if err != nil {
		code, err := readVaultError(err.Error())
		return common.Cluster{}, code, err
	}
	if cluster == nil {
		return common.Cluster{}, http.StatusBadRequest, fmt.Errorf("cluster '%s' not found", id)
	}
	result := common.Cluster{
		ID:               id,
		Name:             cluster.Data["name"].(string),
		DefaultNamespace: cluster.Data["default_namespace"].(string),
		Config:           cluster.Data["config"].(string),
		Host:             cluster.Data["host"].(string),
		Slug:             cluster.Data["slug"].(string),
	}
	if cluster.Data["admin"] != nil {
		result.Admin = cluster.Data["admin"].(bool)
	}
	return result, http.StatusOK, nil
}

// EditCluster can be used to a edit/update a cluster in Vault
func (client *Client) EditCluster(owner string, id string, newCluster common.Cluster, token string) (int, error) {
	// Vault's write will overwrite the entire cluster so we need to read it first
	cluster, code, err := client.ReadCluster(owner, id, token)
	if err != nil {
		return code, err
	}
	// Assign new attributes
	if len(newCluster.Name) > 0 {
		cluster.Name = newCluster.Name
	}
	if len(newCluster.DefaultNamespace) > 0 {
		cluster.DefaultNamespace = newCluster.DefaultNamespace
	}
	if len(newCluster.Config) > 0 {
		cluster.Config = newCluster.Config
	}
	if len(newCluster.Host) > 0 {
		cluster.Host = newCluster.Host
	}

	// Now convert struct to map
	var clusterMap map[string]interface{}
	jsonCluster, err := json.Marshal(cluster)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	err = json.Unmarshal(jsonCluster, &clusterMap)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	return client.write(clustersPath(owner), id, clusterMap, token)
}

// DeleteCluster is used to delete a cluster from Vault
func (client *Client) DeleteCluster(owner, id, token string) (int, error) {
	userClient, err := client.getUserClient(token)
	if err != nil {
		return readVaultError(err.Error())
	}
	_, err = userClient.Logical().Delete(clustersPath(owner) + id)
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

// GetAllClusters will return all of a user's clusters
func (client *Client) GetAllClusters(owner, token string) (clusters map[string]common.Cluster, code int, err error) {
	clusters = map[string]common.Cluster{}
	allClusters, err := client.Logical().List(clustersPath(owner))
	if err != nil {
		code, err = readVaultError(err.Error())
		return
	}
	if allClusters == nil {
		err = fmt.Errorf("No clusters found for user '%s'", owner)
		code = http.StatusNotFound
		return
	}
	for _, id := range allClusters.Data["keys"].([]interface{}) {
		var cluster common.Cluster
		cluster, code, err = client.ReadCluster(owner, id.(string), token)
		if err != nil {
			return
		}
		clusters[id.(string)] = cluster
	}
	return
}
