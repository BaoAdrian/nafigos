package vault

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/rs/xid"
)

// GetUserLoginToken authenticates a user using JWT or Userpass auth method and
// returns a token to be used for additional Vault interactions
func (client *Client) GetUserLoginToken(username, token string) (string, error) {
	var resp *http.Response
	var err error
	if client.AuthMethod != "keycloak" {
		resp, err = http.Post(
			fmt.Sprintf("%s/v1/auth/userpass/login/%s", client.Address(), username),
			"application/json",
			bytes.NewBuffer([]byte(fmt.Sprintf(`{"password": "%s"}`, token))),
		)
	} else {
		resp, err = http.Post(
			fmt.Sprintf("%s/v1/auth/jwt/login", client.Address()),
			"application/json",
			bytes.NewBuffer([]byte(fmt.Sprintf(`{"jwt": "%s", "role": "%s"}`, token, username))),
		)
	}
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	out := struct {
		Auth map[string]interface{} `json:"auth"`
	}{}
	err = json.Unmarshal(body, &out)
	if err != nil {
		return "", err
	}
	if out.Auth == nil {
		return "", fmt.Errorf("no token returned from login")
	}
	return out.Auth["client_token"].(string), err
}

// WriteUser will save a user and secrets/clusters in Vault
func (client *Client) WriteUser(user common.User, token string) (int, error) {
	// Get map from the user struct
	userBytes, err := json.Marshal(user)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	var userMap map[string]interface{}
	err = json.Unmarshal(userBytes, &userMap)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	// Delete some things from map that won't be saved to /info
	delete(userMap, "secrets")
	delete(userMap, "clusters")
	delete(userMap, "password")
	// Write user info to nafigos/user/info
	code, err := client.write(
		namespace+user.Username,
		"/info",
		userMap,
		token,
	)
	if err != nil {
		return code, err
	}
	for _, secret := range user.Secrets {
		// Check if secret exists
		_, _, err = client.ReadSecret(user.Username, secret.ID, token)
		if err == nil {
			// Edit existing secret
			code, err = client.EditSecret(user.Username, secret.ID, secret, token)
			if err != nil {
				return code, err
			}
		} else {
			// Create new secret
			var secretMap map[string]interface{}
			jsonSecret, err := json.Marshal(secret)
			if err != nil {
				return http.StatusInternalServerError, err
			}
			err = json.Unmarshal(jsonSecret, &secretMap)
			if err != nil {
				return http.StatusInternalServerError, err
			}
			code, err = client.AddSecret(user.Username, xid.New().String(), secretMap, token)
			if err != nil {
				return code, err
			}
		}
	}
	// cluster
	for _, cluster := range user.Clusters {
		// Check if secret exists
		_, _, err = client.ReadCluster(user.Username, cluster.ID, token)
		if err == nil {
			// Edit existing cluster
			code, err = client.EditCluster(user.Username, cluster.ID, cluster, token)
			if err != nil {
				return code, err
			}
		} else {
			// Create new cluster
			code, err = client.AddCluster(user.Username, xid.New().String(), cluster, token)
			if err != nil {
				return code, err
			}
		}
	}
	return http.StatusOK, nil
}

// DeleteUser will delete a user's path and policy from Vault
func (client *Client) DeleteUser(username string) (int, error) {
	// Delete user mount
	err := client.Sys().Unmount(namespace + username)
	if err != nil {
		return readVaultError(err.Error())
	}
	// Delete policy
	err = client.Sys().DeletePolicy(namespace + username)
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

// ReadUser will act similar to an Unmarshal function for reading a User struct from Vault
func (client *Client) ReadUser(owner, token string) (user common.User, code int, err error) {
	// First make sure the user exists
	paths, err := client.Logical().List(namespace + owner)
	if err != nil {
		code = http.StatusInternalServerError
		return
	}
	if paths == nil {
		code = http.StatusNotFound
		err = fmt.Errorf("user '%s' not found", owner)
		return
	}
	user.Username = owner
	secrets, code, err := client.GetAllSecrets(owner, token)
	if err != nil && err.Error() != fmt.Sprintf("No secrets found for user '%s'", owner) {
		return
	}
	user.Secrets = secrets

	clusters, code, err := client.GetAllClusters(owner, token)
	if err != nil && err.Error() != fmt.Sprintf("No clusters found for user '%s'", owner) {
		return
	}
	user.Clusters = clusters

	// Now read user info from nafigos/username/info
	infoMap, code, err := client.GetUserInfo(owner, token)
	if err != nil {
		return
	}
	// Convert map to bytes then unmarshal into the user
	infoBytes, err := json.Marshal(infoMap)
	if err != nil {
		code = http.StatusInternalServerError
		return
	}
	err = json.Unmarshal(infoBytes, &user)
	if err != nil {
		code = http.StatusInternalServerError
		return
	}
	code = http.StatusOK
	err = nil
	return
}

// GetUserInfo returns the user's non-secret info from Vault
func (client *Client) GetUserInfo(owner, token string) (result map[string]interface{}, code int, err error) {
	userClient, err := client.getUserClient(token)
	if err != nil {
		code, err = readVaultError(err.Error())
		return
	}
	info, err := userClient.Logical().Read(namespace + owner + "/info")
	if err != nil {
		code, err = readVaultError(err.Error())
		return
	}
	result = info.Data
	return
}

// ListAllUsers will return a list of usernames for all users with secrets in Vault
func (client *Client) ListAllUsers() (users []string, code int, err error) {
	allMounts, err := client.Sys().ListMounts()
	if err != nil {
		code, err = readVaultError(err.Error())
		return
	}
	for path := range allMounts {
		parts := strings.Split(path, "/")
		if len(parts) > 1 && parts[0] == "nafigos" {
			users = append(users, parts[1])
		}
	}
	code = http.StatusOK
	return
}
