package types

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/cyverse/nafigos/common"

	argo "github.com/argoproj/argo/pkg/apis/workflow/v1alpha1"
	core "k8s.io/api/core/v1"
	"sigs.k8s.io/yaml"
)

const defaultSidecarImage = "nafigostest/nafigos-knative-sidecar"

var sidecarImage string

func init() {
	sidecarImage = os.Getenv("SIDECAR_IMAGE_NAME")
	if len(sidecarImage) == 0 {
		sidecarImage = defaultSidecarImage
	}
}

// ArgoWorkflow is a simple wrapper for the Workflow type from Argo. A struct is
// used instead of type alias because a pointer is required for the argo.Workflow
type ArgoWorkflow struct {
	Workflow *argo.Workflow
}

// NewArgoWorkflow is a constructor to create an ArgoWorkflow type of NafigosWorkflow
// from a []byte, which is the workflow defined in the 'wfd.yaml' file
func NewArgoWorkflow(parsedWorkflow []byte) (NafigosWorkflow, error) {
	var wf *argo.Workflow
	err := yaml.Unmarshal(parsedWorkflow, &wf)
	if err != nil {
		return ArgoWorkflow{}, err
	}
	// Read GenerateName since it is excluded when reading from Workflow
	var metadata *struct {
		Metadata struct {
			GenerateName string `yaml:"generateName"`
		} `yaml:"metadata"`
	}
	err = yaml.Unmarshal(parsedWorkflow, &metadata)
	if err != nil {
		return ArgoWorkflow{}, err
	}
	wf.GenerateName = metadata.Metadata.GenerateName
	if len(wf.Name) == 0 {
		wf.Name = strings.TrimRight(wf.GenerateName, "-")
	}
	return ArgoWorkflow{wf}, nil
}

// Type returns the name of this Workflow Type
func (argoWorkflow ArgoWorkflow) Type() string {
	return "ArgoWorkflow"
}

// Run will create and deploy all the necessary Kubernetes resources for this type
// of Workflow. These are the resources created by this function:
//    - workflows.argoproj.io
//    - deployments.apps
func (argoWorkflow ArgoWorkflow) Run(clientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	runID := request["id"].(string)
	version := request["version"].(string)

	// Run Argo Workflow
	argoWorkflow.Workflow.Name = name
	argoWorkflow.Workflow.GenerateName = argoWorkflow.Workflow.Name + "-"
	argoWorkflow.Workflow.ObjectMeta.Labels = map[string]string{
		"app":        name,
		"version":    version,
		"nafigos_id": runID,
	}
	_, err := clientsets.CreateArgoWorkflow(argoWorkflow.Workflow, namespace)
	if err != nil {
		_, err = clientsets.UpdateArgoWorkflow(argoWorkflow.Workflow, namespace)
	}
	if err != nil {
		return fmt.Errorf("Unable to create Argo Workflow '%s': %v", name, err)
	}

	// Create sidecar Deployment
	sidecarDeployment := createDeployment([]core.Container{{
		Name:  common.FixName(sidecarImage),
		Image: sidecarImage,
		Env: []core.EnvVar{
			{
				Name:  "PODNAME",
				Value: strings.TrimRight(argoWorkflow.Workflow.GenerateName, "-"),
			},
		},
		Ports: []core.ContainerPort{{
			ContainerPort: int32(80),
			Name:          "http",
		}},
	}}, runID, name, version)
	_, err = clientsets.CreateDeployment(sidecarDeployment, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`deployments.apps "%s" already exists`, sidecarDeployment.Name) {
		_, err = clientsets.UpdateDeployment(sidecarDeployment, namespace)
	}
	if err != nil {
		return fmt.Errorf("Error creating Sidecar Deployment '%s': %v", name, err)
	}

	// Create Service
	service := createService(name, runID, version, append(request["httpPorts"].([]core.ServicePort), request["tcpPorts"].([]core.ServicePort)...))
	_, err = clientsets.CreateService(service, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`services "%s" already exists`, service.Name) {
		_, err = clientsets.UpdateService(service, namespace)
	}
	if err != nil {
		return fmt.Errorf("Error creating Service: %v", err)
	}
	return err
}

// SetupRouting ...
func (argoWorkflow ArgoWorkflow) SetupRouting(clientsets *common.K8sClientsets, adminClientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string, cluster common.Cluster) error {
	err := standardIstioRouting(clientsets, adminClientsets, request, name, namespace, cluster)
	if err != nil {
		return fmt.Errorf("Error creating Istio routing: %v", err)
	}
	return nil
}

// Delete will delete the resources created by the Run method
func (argoWorkflow ArgoWorkflow) Delete(clientsets *common.K8sClientsets, name, namespace string) error {
	err := clientsets.DeleteArgoWorkflow(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteService(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteDeployment(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteVirtualService(name, namespace)
	if err != nil {
		return err
	}
	err = clientsets.DeleteDestinationRule(name, namespace)
	if err != nil {
		return err
	}
	return err
}

// ScaleFromZero will re-run a Workflow that has completed and scaled down
func (argoWorkflow ArgoWorkflow) ScaleFromZero(clientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	return argoWorkflow.Run(clientsets, request, name, namespace)
}
