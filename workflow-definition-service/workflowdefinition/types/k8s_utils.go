package types

import (
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// createDeployment will use Containers defined by a user to create a K8s Deployment
// labeled with the app name and version
func createDeployment(containers []core.Container, id, name, version string) *apps.Deployment {
	return &apps.Deployment{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":        name,
				"version":    version,
				"nafigos_id": id,
			},
		},
		Spec: apps.DeploymentSpec{
			Selector: &meta.LabelSelector{
				MatchLabels: map[string]string{
					"app":        name,
					"version":    version,
					"nafigos_id": id,
				},
			},
			Template: core.PodTemplateSpec{
				ObjectMeta: meta.ObjectMeta{
					Name: name,
					Labels: map[string]string{
						"app":        name,
						"version":    version,
						"nafigos_id": id,
					},
				},
				Spec: core.PodSpec{
					Containers: containers,
				},
			},
		},
	}
}

// createService creates a K8s Service to expose container ports defined by
// the user for Deployment labelled with app name and version
func createService(name, id, version string, ports []core.ServicePort) *core.Service {
	return &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":        name,
				"service":    name,
				"version":    version,
				"nafigos_id": id,
			},
		},
		Spec: core.ServiceSpec{
			Ports: ports,
			Selector: map[string]string{
				"app":        name,
				"version":    version,
				"nafigos_id": id,
			},
		},
	}
}
