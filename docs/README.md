# Nafigos

### Table of Contents
[[_TOC_]]


## [Getting Started (Users)](./getting_started_users.md)
## [Getting Started (Developers)](./getting_started_developers.md)
## [Developer Documentation](./developers/README.md)


## Extra Info

Use these commands to get important environment variables when using Kind for local development:

```bash
# with service-cluster context:
export KEYCLOAK_URL=$(kubectl get --context=kind-service-cluster po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-service-cluster get service keycloak -o jsonpath='{.spec.ports[?(@.name=="keycloak")].nodePort}')
export SERVICE_GATEWAY_URL=$(kubectl --context=kind-service-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-service-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export NAFIGOS_API=$SERVICE_GATEWAY_URL

# with user-cluster context:
export USER_GATEWAY_URL=$(kubectl --context=kind-user-cluster get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl --context=kind-user-cluster -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
```
