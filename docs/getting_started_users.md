# Getting Started

This document outlines getting started with Nafigos as a user. This assumed you have an account and access to a running Nafigos server with a connected user cluster.


### Table of Contents
[[_TOC_]]


### Prepare Nafigos CLI
The Nafigos CLI is the simplest way to interact with the Nafigos API.

In order to use the Nafigos CLI, you will need to build it
1. Clone or download the repository from GitLab
	```bash
	git clone https://gitlab.com/cyverse/nafigos.git
	# or download it without git:
	wget https://gitlab.com/cyverse/nafigos/-/archive/master/nafigos-master.zip
	unzip nafigos.zip
	```
2. Build the `nafigos` binary:
	```bash
	cd nafigos/cmd
	go build -o nafigos
	```
3. Move this to somewhere in your `$PATH`, or add this directory to your path:
	```bash
	sudo mv nafigos /usr/local/bin/nafigos
	# or
	export PATH=$PATH:$(pwd)
	```


### First Steps

In order to use the Nafigos CLI, you will need to define the following environment variables:
```bash
export NAFIGOS_API=https://ca.cyverse.org
export NAFIGOS_TOKEN=<token>
```

You can get this token by visiting https://ca.cyverse.org/user/login and logging in with your CyVerse credentials. Then, copy the `IDToken` into the `NAFIGOS_TOKEN` environment variable.


### User Actions
Now you have everything you need to interact with your user's resources in Nafigos, specifically the Kubernetes cluster info and secrets. By default, you should already have a cluster provided by CyVerse.

```bash
nafigos get user ${username}

# list all secrets
nafigos get secret ${username}
# get a specific secret
nafigos get secret ${username} ${secretID}
# delete a secret
nafigos delete secret ${username} ${secretID}
# create a new secret (type must be git, dockerhub, or gcr) 
nafigos create secret -type ${type} -username ${username} -value ${token_secret}
# create a new secret from JSON file
nafigos create secret -f secret.json

# list all clusters
nafigos get cluster
# get a specific cluster
nafigos get cluster ${clusterID}
# delete a cluster
nafigos delete cluster ${clusterID}
# create a new kclsuter
nafigos create cluster -config <base64_kubeconfig> -name ${name} -default_namespace ${default_namespace}
# create a new cluster from JSON file
nafigos create cluster -f cluster.json

# use this to get your Cluster ID if you have 1 cluster
export clusterID=$(nafigos get cluster | jq -r 'keys'[0])
```

### Create a WorkflowDefinition

First, check out [this document](workflowdefinition/README.md) for more information on creating your `wfd.yaml` file.

Now, you are ready to create your WorkflowDefinition in Nafigos:
```bash
nafigos create wfd -url ${git_url} -branch ${git_branch}
# Note: -branch can be excluded to use default 'master'

# use this to create a WorkflowDefinition and save it's ID
export wfdID=$(nafigos create wfd -url ${git_url} -branch ${git_branch} | jq -r .id)
```

A few examples of WorkflowDefinition repos to try:
- https://gitlab.com/cyverse/nafigos-helloworld
- https://gitlab.com/cyverse/nafigos-simple-http-counter
- https://gitlab.com/cyverse/nafigos-postgres
- https://gitlab.com/cyverse/nafigos-helloworld-argo

Now that you have a WorkflowDefinition, you can use the following commands:
```bash
# list all of your WorkflowDefinitions
nafigos get wfd
# get information about a specific WorkflowDefinition
nafigos get wfd ${wfdID}
# delete a WorkflowDefinition
nafigos delete wfd ${wfdID}
```


### Build

First, you will need to create a Docker Hub or Google Container Registry secret for pushing your built image. Follow [this guide](https://docs.docker.com/docker-hub/access-tokens/) to create an access token for Docker Hub, then use the commands above to create the secret, making sure to save the ID (you can find it again by listing all secrets: `nafigos get secret ${username}`).

Use the Nafigos CLI to get the IDs for your cluster and container registry secret (see section above titled ["User Actions"](#user-actions)). Then, use the following command to initiate a build:

```bash
nafigos build -registry-secret ${secretID} -cluster ${clusterID} ${wfdID}
```
  - You can also specify a `-git-secret` for private repositories or `-namespace` to choose the namespace to build in


### Run

Once again, use the Nafigos CLI to get the ID for your cluster and then use the following command:

```bash
nafigos run -cluster ${clusterID} ${wfdID}
# Use this to start a run and save its ID
export runID=$(nafigos run -cluster ${clusterID} ${wfdID} | jq -r .id)
```
  - You can also use `-namespace` to choose the namespace to run in
  - `-disable-auth` can be used to disable the auth requirement for accessing the running workflow. This is recommended if you intend to access the workflow in a web browser

You can now use the Run ID for additional commands:
```bash
nafigos get run
nafigos get run ${runID}
nafigos delete run ${runID}
```

You will be able to access this run at:
```bash
https://username.ca-run.cyverse.org/runID:port
```

Due to SSL issues, you currently have to use the `-k` or `--insecure` flag with `curl` to hit your endpoint.  You also need to ensure that you have an `Authorization` header set to access the endpoint.

Putting it all together, your `curl` command should look like:

```
curl -k -H "Authorization: $NAFIGOS_TOKEN" https://username.ca-run.cyverse.org/runID:port
```
