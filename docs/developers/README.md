# Developer Documentation

### Table of Contents
[[_TOC_]]


### [API Service](./api-service.md)
### [Build Service](./build-service.md)
### [Debugging](./debug.md)
### [Keycloak setup](./keycloak.md)
### [Logging and Error Conventions in Nafigos](./logs_and_errors.md)
### [Phylax](./phylax.md)
### [Vault](./vault.md)
### [WorkflowDefinition Service](./workflow-definition-service.md)


### Overview

Nafigos is composed of several independent microservices that use NATS for communication.  Users communicate
with Nafigos using REST calls to the API service which in turn sends messages on NATS.  Each microservice subscribes
to the subjects relevant to its functionality and handles the requests internally.  Workflow definitions are stored in
hosted Git solutions like Gitlab and Github and are then parsed and cached in a MongoDB database for quicker access.  The
Phylax service monitors the operations of the application and keeps a record in MongoDB as well.  It does this by "eavesdropping"
on subjects for other services and stored records in MongoDB.  When Workflows are ready to be run, the Pleo service communicates
with the specified external Kubernetes cluster (which we broadly call the "User Cluster") to create the necessary Kuberentes 
resources.

![Overview of the Nafigos architecture](nafigos_diagram.png)
