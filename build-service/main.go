package main

import (
	"os"
	"sync"

	"gitlab.com/cyverse/nafigos/build-service/build"
	"gitlab.com/cyverse/nafigos/common"

	log "github.com/sirupsen/logrus"
)

const (
	defaultClusterID   = "nafigos-cluster"
	defaultClientID    = "build"
	defaultConnAddress = "nats://localhost:4222"
)

var natsInfo map[string]string

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	natsInfo = common.GetNATSInfo(defaultClusterID, defaultClientID, defaultConnAddress)
}

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingQueueSubscriber("Build.Build", "Build.BuildQueue", build.PreBuild, natsInfo, &wg)
	wg.Wait()
}
