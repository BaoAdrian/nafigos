# Install

### Table of Contents
[[_TOC_]]


## Getting Started

This directory is reserved for ansible and other resources needed to deploy the Nafigos platform and user clusters.

### Prerequisites

0. Ubuntu 18.04 and newer are currently the only supported operating systems.
1. [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu)
2. [Install Docker](https://docs.docker.com/engine/install/ubuntu/)
3. Ensure that you can [manage Docker as a non-root user](https://docs.docker.com/engine/install/linux-postinstall/)

### Ansible setup

There are three playbooks:

* `playbook-kubespray.yml` - used to install a multinode kubernetes cluster using kubespray
* `playbook-nafigos.yml` - used to install nafigos, and depending on the values in config.yml, it will also setup a development environment (currently using kind)
* `playbook-vault.yml` - used to install Vault on a separate server with PostgreSQL storage backend

Note: you should ensure that you have ansible (and ansible-galaxy) installed on the system that you'll run ansible on.

1. Install `requirements.yml`: If you are setting up a new development environment, you should install dependencies in `requirements.yml` from the host that will be running ansible
    ```bash
    ansible-galaxy role install -r requirements.yml
    ```

2. For remote deployments, copy `example_hosts.yml` to `hosts.yml`
    ```bash
    cp example_hosts.yml hosts.yml
    ```

## Nafigos Install

### Local development

To set up a local development environment, run the `local_install.sh` script in this directory.  It will generate a suitable `config.yml` file for local development and prompt you for a few configuration parameters.  Hit "Enter" to accept the default values in the brackets.

```
$ ./local_install.sh
patching file config.yml
Enter the URL to the Git remote repo [git@gitlab.com:simpsonw/nafigos.git]:
Enter the Git branch you want to use [master]:
Enter the path to the Nafigos source code directory [/home/will/workspace/golang/src/gitlab.com/cyverse/nafigos]:
```

Next it will run the Ansible playbook to install the necessary dependencies for Nafigos and provision the user and services cluster with [k3d](https://github.com/rancher/k3d), a tool for running the [k3s](https://github.com/rancher/k3s) Kubernetes distribution in Docker.

Once the script has finished, go to the project root. Ensure that you have the service cluster selected as your current context:

```
kubectl config use-context k3d-service-cluster
```

Now run `skaffold dev` to deploy Nafigos to your local Kubernetes cluster.  If you get an error message saying the deployments didn't  stablize in time, try running `skaffold dev --status-check=false`

### Other environments

1. `config.yml`: copy `example_config.yml` to `config.yml` if it's a new deployment; edit `config.yml` as necessary
    ```bash
    cp example_config.yml config.yml
    ```
2. To start the nafigos install process for remote hosts, execute the ansible-playbook
    ```bash
    ansible-playbook -i hosts.yml playbook-nafigos.yml
    ```

    For local development, use `localhost.yml`
    ```bash
    ansible-playbook -K -i localhost.yml playbook-nafigos.yml
    ```

## Vault Install

The `playbook-vault.yml` playbook is intended to be used separate from the other Nafigos playbooks. This is used to install Vault on a separate server (not a K8s cluster) alongside a PostgreSQL backend. The PostgreSQL database must be installed and setup separately, but this playbook will create the necessary tables required by Vault. Once the playbook completes, Vault will be installed, running, and unsealed. The unseal keys and root token are saved in `vault_secrets.json`.

1. Make sure to setup the `vault_host` in `hosts.yml`

2. Copy the default Vault config file:
    ```bash
    cp example_config_vault.yml vault_config.yml
    ```

3. Edit the variables, making sure to use the correct secure password for PostgreSQL
    - If you require TLS for Vault (**which should be used for production**), make sure to enable it and include cert and key data like this:
        ```yaml
        VAULT_TLS_KEYDATA: |-
          -----BEGIN RSA PRIVATE KEY-----
          MIIEpAIBAAKCAQEA0LJAufQglnu6jq5RPL+5o3dc/mowFH4NlFyQ55I/4xc9msqn
          ...
        ```

4. Run the Playbook:
    ```bash
    ansible-playbook -i hosts.yml playbook-vault.yml
    ```

5. **Save the `vault_secrets.json` file** and use the root token as the `VAULT_TOKEN` variable in your Nafigos `config.yml`
