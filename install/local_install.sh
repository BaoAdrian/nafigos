#!/bin/bash

if ! dpkg -s ansible > /dev/null 22>&1 ; then

    sudo apt update
    sudo apt install software-properties-common
    sudo apt-add-repository --yes --update ppa:ansible/ansible
    sudo apt install ansible=2.8.5-1ppa~bionic --yes

fi

if [ ! -f config.yml ] ; then
    cp example_local_config.yml config.yml

    DEFAULT_GIT_URL=`git remote get-url origin`
    read -p "Enter the URL to the Git remote repo [$DEFAULT_GIT_URL]: " GIT_URL
    GIT_URL=${GIT_URL:-$DEFAULT_GIT_URL}
    export GIT_URL

    DEFAULT_GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    read -p "Enter the Git branch you want to use [$DEFAULT_GIT_BRANCH]: " GIT_BRANCH
    GIT_BRANCH=${GIT_BRANCH:-$DEFAULT_GIT_BRANCH}
    export GIT_BRANCH

    DEFAULT_SRC_DIR=`dirname $PWD`
    read -p "Enter the path to the Nafigos source code directory [$DEFAULT_SRC_DIR]: " SRC_DIR
    SRC_DIR=${SRC_DIR:-$DEFAULT_SRC_DIR}
    export SRC_DIR

    envsubst < config.yml > config_subst.yml
    mv config_subst.yml config.yml
fi

ansible-galaxy role install -r requirements.yml
ansible-playbook -K -i localhost.yml playbook-nafigos.yml

printf "Setup complete! \n"
printf "If you don't see any failures, go to NAFIGOS_SRC_DIR and run 'skaffold_dev' to start Nafigos\n"
cat config.yml | grep NAFIGOS_SRC_DIR:
