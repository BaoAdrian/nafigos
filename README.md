# nafigos
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/cyverse/nafigos?style=flat-square)](https://goreportcard.com/report/gitlab.com/cyverse/nafigos)
[![Pipeline](https://gitlab.com/cyverse/nafigos/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/cyverse/nafigos/commits/master)
[![coverage](https://gitlab.com/cyverse/nafigos/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/cyverse/nafigos/commits/master)

Nafigos is a project enabling Continuous Analysis on Kubernetes clusters.

### Table of Contents
[[_TOC_]]


## [Getting Started](docs/README.md)

### [Developers](docs/getting_started_developers.md)
This document outlines the process for getting started as a Nafigos developer, including local development setup and basic tasks for using Nafigos.


### [Users](docs/getting_started_users.md)
This document has some basic information for new users, such as installing the CLI, basic actions to use Nafigos with the CLI, and a link to WorkflowDefinition file documentation. Then there is also some information about building and running Workflows.


### [Workflow Definition](docs/workflowdefinition/README.md)
This holds some documentation about the WorkflowDefinition YAML files and the different WorkflowDefinition types.


#### [Container Workflow](docs/workflowdefinition/container_workflow.md)
This is the basic Workflow Type for Nafigos and just runs some containers in a Pod.


#### [Argo Workflow](docs/workflowdefinition/argo_workflow.md)
This is a more robust Workflow Type leveraging the powerful [Argo Workflows](https://github.com/argoproj/argo).


## [Developer Documentation](docs/developers/README.md)
This directory contains developer documentation for each service in addition to some other relevant information.


### [API Service](docs/developers/api-service.md)
### [Build Service](docs/developers/build-service.md)
### [Keycloak setup](docs/developers/keycloak.md)
### [Logging and Error Conventions in Nafigos](docs/developers/logs_and_errors.md)
### [Phylax](docs/developers/phylax.md)
### [Vault](docs/developers/vault.md)
### [WorkflowDefinition Service](docs/developers/workflow-definition-service.md)
