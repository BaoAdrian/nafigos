package common

import (
	"strings"
	"sync"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
)

// MonitoringSubject is the subject phylax monitors for creating subscribers
var MonitoringSubject = "Phylax.CreateSubscriber"

// RegisterMonitoring is used to monitor a separate streaming channel
func RegisterMonitoring(subject string, natsInfo map[string]string) {
	log.WithFields(log.Fields{
		"package":  "common",
		"function": "RegisterMonitoring",
		"subject":  subject,
	}).Info("registering monitoring for subject")
	StreamingPublish([]byte(subject), natsInfo, MonitoringSubject, "Common")
}

func getStanConnection(subject string, natsInfo map[string]string) (stan.Conn, error) {
	str := strings.Split(subject, ".")[1]
	return stan.Connect(
		natsInfo["cluster_id"],
		natsInfo["client_id"]+"-"+str,
		stan.NatsURL(natsInfo["address"]),
		stan.ConnectWait(5*time.Second),
	)
}

func getStanOptions(subject string) []stan.SubscriptionOption {
	aw, _ := time.ParseDuration("60s")
	return []stan.SubscriptionOption{
		stan.DurableName("Durable" + subject),
		stan.MaxInflight(25),
		stan.SetManualAckMode(),
		stan.AckWait(aw),
	}
}

// StanSubscriber creates a NATS Streaming Subscriber that will listen on a
// subject and perform asynchronous actions.  The cb parameter can by any function
// which conforms to the stan.MsgHandler type (i.e. accepts *stan.Msg as a parameter)
func StanSubscriber(subject string, cb stan.MsgHandler, natsInfo map[string]string, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "common",
		"function": "StanSubscriber",
		"subject":  subject,
	})

	sc, err := getStanConnection(subject, natsInfo)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("unable to connect to NATS Streaming")
	}

	subOptions := getStanOptions(subject)
	logger.Trace("listening started")
	_, err = sc.Subscribe(subject, cb, subOptions...)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to subscribe on subject '%s'", subject)
	}
	wg.Wait()
}

// StanQueueSubscriber creates a NATS Streaming Subscriber that will listen on a
// subject as a queue subscriber and perform asynchronous actions.  The cb
// parameter can by any function which conforms to the stan.MsgHandler type
// (i.e. accepts *stan.Msg as a parameter).
// For information on queue subscribers, see
// https://docs.nats.io/developing-with-nats-streaming/queues
func StanQueueSubscriber(subject string, queue string, cb stan.MsgHandler, natsInfo map[string]string, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "common",
		"function": "StanQueueSubscriber",
		"subject":  subject,
		"queue":    queue,
	})

	sc, err := getStanConnection(subject, natsInfo)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("unable to connect to NATS Streaming")
	}

	subOptions := getStanOptions(subject)
	logger.Trace("listening started")
	_, err = sc.QueueSubscribe(subject, queue, cb, subOptions...)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to subscribe to queue on subject '%s'", subject)
	}
	wg.Wait()
}

// StreamingSubscriber creates a NATS Streaming  Subscriber that will listen on a
// subject and perform asynchronous actions.  The action function should accept a
// []byte that represents JSON data, and a map[string]string, which represents NATS
// connection info.
func StreamingSubscriber(subject string, action func([]byte, map[string]string), natsInfo map[string]string, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "common",
		"function": "StreamingSubscriber",
		"subject":  subject,
	})
	i := 0
	requestHandler := func(msg *stan.Msg) {
		msg.Ack()
		i++
		req, err := GetRequestFromCloudEvent(msg.Data)
		if err == nil {
			logger.WithFields(log.Fields{"request": string(req)}).Trace("received message")
			action(req, natsInfo)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		}
	}

	StanSubscriber(subject, requestHandler, natsInfo, wg)
}

// StreamingQueueSubscriber creates a NATS Streaming Queue Subscriber that will listen on a
// subject and perform asynchronous actions.  The action function should accept a
// []byte that represents JSON data, and a map[string]string, which represents NATS
// connection info.
func StreamingQueueSubscriber(subject string, queue string, action func([]byte, map[string]string), natsInfo map[string]string, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "common",
		"function": "StreamingQueueSubscriber",
		"subject":  subject,
		"queue":    queue,
	})
	requestQueueHandler := func(msg *stan.Msg) {
		msg.Ack()
		req, err := GetRequestFromCloudEvent(msg.Data)
		if err == nil {
			logger.WithFields(log.Fields{"request": string(req)}).Trace("received message")
			action(req, natsInfo)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		}
	}

	StanQueueSubscriber(subject, queue, requestQueueHandler, natsInfo, wg)
}

// SynchronousSubscriber is used to synchronously respond to requests
func SynchronousSubscriber(subject string, queue string, action func([]byte, *nats.Msg, map[string]string), natsInfo map[string]string, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "common",
		"function": "SynchronousSubscriber",
		"subject":  subject,
		"queue":    queue,
	})

	// Connect to NATS
	nc, err := nats.Connect(natsInfo["address"])
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("unable to connect to NATS")
	}
	defer nc.Close()

	logger.Trace("listening started")
	nc.QueueSubscribe(subject, queue, func(msg *nats.Msg) {
		request, err := GetRequestFromCloudEvent(msg.Data)
		if err == nil {
			logger.WithFields(log.Fields{"request": string(request)}).Trace("received message")
			action(request, msg, natsInfo)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		}
	})
	wg.Wait()
}

// StreamingTestSubscriber is the same as StreamingSubscriber but will exit after
// receiving and processing one request
func StreamingTestSubscriber(subject string, queue string, action func([]byte, map[string]string), natsInfo map[string]string, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "common",
		"function": "StreamingTestSubscriber",
		"subject":  subject,
		"queue":    queue,
	})

	str := strings.Split(subject, ".")[1]
	sc, err := stan.Connect(
		natsInfo["cluster_id"],
		natsInfo["client_id"]+"-"+str,
		stan.NatsURL(natsInfo["address"]),
		stan.ConnectWait(5*time.Second),
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("unable to connect to NATS Streaming")
	}

	aw, _ := time.ParseDuration("60s")
	subOptions := []stan.SubscriptionOption{
		stan.DurableName("Durable" + subject),
		stan.MaxInflight(25),
		stan.SetManualAckMode(),
		stan.AckWait(aw),
		stan.DeliverAllAvailable(),
	}

	logger.Trace("listening started")
	sc.QueueSubscribe(subject, queue, func(msg *stan.Msg) {
		msg.Ack()
		req, err := GetRequestFromCloudEvent(msg.Data)
		if err == nil {
			logger.WithFields(log.Fields{"request": string(req)}).Trace("received message")
			action(req, natsInfo)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		}
		wg.Done()
	}, subOptions...)
	wg.Wait()
}

// SynchronousTestSubscriber is the same as SynchronousSubscriber but will exit after
// receiving and processing one request
func SynchronousTestSubscriber(subject string, queue string, action func([]byte, *nats.Msg, map[string]string), natsInfo map[string]string, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "common",
		"function": "SynchronousTestSubscriber",
		"subject":  subject,
		"queue":    queue,
	})

	// Connect to NATS
	nc, err := nats.Connect(natsInfo["address"])
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("unable to connect to NATS")
	}
	defer nc.Close()

	logger.Trace("listening started")
	nc.QueueSubscribe(subject, queue, func(msg *nats.Msg) {
		request, err := GetRequestFromCloudEvent(msg.Data)
		if err == nil {
			logger.WithFields(log.Fields{"request": string(request)}).Trace("received message")
			action(request, msg, natsInfo)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		}
		wg.Done()
	})
	wg.Wait()
}
