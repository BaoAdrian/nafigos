package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"sync"
	"time"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/cs-service/cs"

	"github.com/nats-io/stan.go"
)

const (
	defaultClusterID   = "nafigos-cluster"
	defaultClientID    = "cs"
	defaultConnAddress = "nats://localhost:4222"
)

var natsInfo map[string]string

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	/*err = cs.InitMongoDB(
		os.Getenv("MONGODB_DB_NAME"),
		os.Getenv("MONGODB_ADDRESS"),
	)
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "main",
			"function": "init",
			"error":    err,
		}).Panic("unable to connect to MongoDB")
	}*/
	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("successfully connected to MongoDB")

	t := time.Now()
	clientID := fmt.Sprintf("%s-%d", defaultClientID, t.Unix())
	natsInfo = common.GetNATSInfo(defaultClusterID, clientID, defaultConnAddress)
}

func main() {
	monitoringSubjects := map[string]stan.MsgHandler{
		"WorkflowDefinition.Create":   cs.WorkflowDefinitionCreate,
		"WorkflowDefinition.Finished": cs.WorkflowDefinitionFinished,
	}
	var wg sync.WaitGroup
	wg.Add(1 + len(monitoringSubjects))
	for subject, handler := range monitoringSubjects {
		go common.StanSubscriber(subject, handler, natsInfo, &wg)
	}
	go common.SynchronousSubscriber("Anchore.Get", "Anchore.GetQueue", cs.GetReport, natsInfo, &wg)
	wg.Wait()
}
