package cs

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/workflow-definition-service/workflowdefinition"
)

const (
	defaultClusterID       = "nafigos-cluster"
	defaultClientID        = "workflow-definition"
	defaultConnAddress     = "nats://localhost:4222"
	defaultAnchoreURL      = "http://anchore-anchore-engine-api.default.svc.cluster.local:8228/v1/"
	defaultAnchoreUser     = "admin"
	defaultAnchorePassword = "foobar"
)

// WorkflowResponse struct is used to parse through PublishRequest result
type WorkflowResponse struct {
	Error               *common.Error
	WorkflowDefinitions []workflowdefinition.WorkflowDefinition `json:"data"`
}

// WFDReports has list of all reports
type WFDReports struct {
	Reports []ImageReport
}

// ImageReport is the vulnerability report of a single image
type ImageReport struct {
	ImageID           string          `json:"imageID"`
	ImageDigest       string          `json:"imageDigest"`
	Vulnerabilities   []Vulnerability `json:"vulnerabilities"`
	VulnerabilityType string          `json:"vulnerability_type"`
}

// Vulnerability struct has metadata of found vulnerability
type Vulnerability struct {
	Feed           string `json:"feed"`
	FeedGroup      string `json:"feed_group"`
	Fix            string `json:"fix"`
	NvdData        []nvd  `json:"nvd_data"`
	Package        string `json:"package"`
	PackageCPE     string `json:"package_cpe"`
	PackageCPE23   string `json:"package_cpe23"`
	PackageName    string `json:"package_name"`
	PackagePath    string `json:"package_path"`
	PackageType    string `json:"package_type"`
	PackageVersion string `json:"package_version"`
	Severity       string `json:"severity"`
	URL            string `json:"url"`
	VendorData     []nvd  `json:"vendor_data"`
	Vuln           string `json:"vuln"`
}

// nvd provides the id of the vulnerabilities and the CVE data
type nvd struct {
	ID     string `json:"id"`
	CvssV2 data   `json:"cvss_v2"`
	CvssV3 data   `json:"cvss_v3"`
}

// data struct labels the different score of the vulnerability found
type data struct {
	BaseScore           float32 `json:"base_score"`
	ExploitabilityScore float32 `json:"exploitability_score"`
	ImpactScore         float32 `json:"impact_score"`
}

var anchoreInfo = GetAnchoreInfo()

// WorkflowDefinitionCreate logs when a WorkflowDefinition is created
func WorkflowDefinitionCreate(msg *stan.Msg) {
	msg.Ack()
	log.WithFields(log.Fields{
		"package":        "cs",
		"function":       "WorkflowDefinitionCreate",
		"wfd_create_msg": msg,
	}).Info("received message on subject WorkflowDefinitionCreate")
}

// WorkflowDefinitionFinished is used to parse through Workflow, 
// retrieve docker image id's then analyze docker image using Anchore
func WorkflowDefinitionFinished(msg *stan.Msg) {
	msg.Ack()
	log.WithFields(log.Fields{
		"package":        "cs",
		"function":       "WorkflowDefinitionFinished",
		"wfd_create_msg": msg,
	}).Info("received message on subject WorkflowDefinitionFinished")
	reqBytes, err := common.GetRequestFromCloudEvent(msg.Data)
	if err != nil {
		log.Println("Error getting request from cloud event:", err)
		return
	}

	natsInfo := common.GetNATSInfo(defaultClusterID, defaultClientID, defaultConnAddress)
	var request workflowdefinition.Request
	json.Unmarshal(reqBytes, &request)

	result, err := common.PublishRequest(&request, "WorkflowDefinition.GetForUser", "Container Security", natsInfo["address"])
	if err != nil {
		log.Println("Error getting WorkflowDefinition:", err)
		return
	}

	var workFlows WorkflowResponse
	json.Unmarshal(result, &workFlows)

	if workFlows.Error != nil {
		log.Println("Error getting WorkflowDefinition:", err)
		return
	}

	numWorkflows := len(workFlows.WorkflowDefinitions)
	CurrentWFDIndex := numWorkflows - 1

	// AnchoreAction first time is used to add image to Anchore Engine to be analyzed
	AnchoreAction(workFlows, CurrentWFDIndex, "add")

	// AnchoreAction second time is used to wait for an image to be analyzed
	AnchoreAction(workFlows, CurrentWFDIndex, "wait")
}

// AnchoreAction will perform in action using the anchore-cli. Currenlty used to 'add' and 'wait' on images
func AnchoreAction(workFlows WorkflowResponse, CurrentWFDIndex int, action string){
	ImageLength := len(workFlows.WorkflowDefinitions[CurrentWFDIndex].Build)
	for i := 0; i < ImageLength; i++ {
		_, err := exec.Command("anchore-cli", "--json", "--url", anchoreInfo["url"], "--u", anchoreInfo["user"], "--p", anchoreInfo["password"], "image", action, workFlows.WorkflowDefinitions[CurrentWFDIndex].Build[i].Image).Output()
		if err != nil {
			log.Fatal(err)
		}
	}
}

// CheckWorkflowID Checks if workflow ID exists
func CheckWorkflowID(workflowID string, workFlows WorkflowResponse) (int, bool) {
	var index int
	var exists bool
	numWorkflows := len(workFlows.WorkflowDefinitions)
	for i := 0; i < numWorkflows; i++ {
		if workflowID == workFlows.WorkflowDefinitions[i].ID {
			index = i
			exists = true
			break
		}
	}
	return index, exists
}

// GetWorkflowImages Returns a List of images in the provided Workflow
func GetWorkflowImages(workFlow workflowdefinition.WorkflowDefinition) []string {
	var Images []string
	ImageLength := len(workFlow.Build)
	for i := 0; i < ImageLength; i++ {
		Images = append(Images, workFlow.Build[i].Image)
	}
	return Images
}

// GetImageAnalysis get analysis of provided image(s)
func GetImageAnalysis(Images []string) []ImageReport {
	var report ImageReport
	var reports []ImageReport
	ImageLength := len(Images)
	for i := 0; i < ImageLength; i++ {
		more, err := exec.Command("anchore-cli", "--json", "--url", anchoreInfo["url"], "--u", anchoreInfo["user"], "--p", anchoreInfo["password"], "image", "vuln", Images[i], "all").Output()
		err = json.Unmarshal(more, &report)
		report.ImageID = Images[i]
		if err != nil {
			log.Fatal(err)
		} else {
			reports = append(reports, report)
		}
	}
	return reports
}

// GetAllImageAnalysis returns all the reports for all images
func GetAllImageAnalysis(workFlows WorkflowResponse) []byte {
	var out []byte
	var wfdReports WFDReports
	var Images []string

	numWorkflows := len(workFlows.WorkflowDefinitions)
	for i := 0; i < numWorkflows; i++ {
		image := GetWorkflowImages(workFlows.WorkflowDefinitions[i])
		Images = append(Images, image...)
		more := GetImageAnalysis(Images)
		wfdReports.Reports = append(wfdReports.Reports, more...)
	}
	out, err := json.Marshal(wfdReports)
	if err != nil {
		panic(err)
	}
	return out
}

// GetReport gets the requested upon Anchore reports
func GetReport(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "cs-service",
		"function": "GetReport",
	})

	var request workflowdefinition.Request
	var workFlows WorkflowResponse

	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	var workflowID = request.GetWorkflowID()
	fmt.Println("workflowID: " + workflowID)

	result, err := common.PublishRequest(&request, "WorkflowDefinition.GetForUser", "Container Security GetReports", natsInfo["address"])
	if err != nil {
		log.Println("Error getting WorkflowDefinition:", err)
		return
	}
	var response []byte

	json.Unmarshal(result, &workFlows)

	if len(workFlows.WorkflowDefinitions) == 0 {
		fmt.Println("No workflows found")
		return
	}

	if len(workflowID) != 0 {
		var wfdReports WFDReports
		index, exists := CheckWorkflowID(workflowID, workFlows)
		if !exists {
			fmt.Println("Not a valid workflow ID")
			return
		}
		workFlow := workFlows.WorkflowDefinitions[index]
		Images := GetWorkflowImages(workFlow)
		test := GetImageAnalysis(Images)
		wfdReports.Reports = append(wfdReports.Reports, test...)
		response, err = json.Marshal(wfdReports)
		if err != nil {
			fmt.Println("Failed to marshall report")
			panic(err)
		}
	} else {
		response = GetAllImageAnalysis(workFlows)
	}
	msg.Respond(response)
}

// GetAnchoreInfo returns the parameters to be used with the Anchore engine
func GetAnchoreInfo() map[string]string {
	anchoreInfo := make(map[string]string)
	anchoreInfo["url"] = os.Getenv("ANCHORE_URL")
	if len(anchoreInfo["url"]) == 0 {
		anchoreInfo["url"] = defaultAnchoreURL
	}
	anchoreInfo["user"] = os.Getenv("ANCHORE_USER")
	if len(anchoreInfo["user"]) == 0 {
		anchoreInfo["user"] = defaultAnchoreUser
	}
	anchoreInfo["password"] = os.Getenv("ANCHORE_PASSWORD")
	if len(anchoreInfo["password"]) == 0 {
		anchoreInfo["password"] = defaultAnchorePassword
	}
	return anchoreInfo
}
