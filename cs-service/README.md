# CS Service

The Container Security Service(CS Service) serves as a background operation that scans and develops vulnerability reports on docker images provided in a workflow definition. This service acts as an intermidiary between Nafigos and Anchore, a container/image analysis tool.

[Read more...](../docs/developers/cs.md)
