package main

import (
	"os"
	"sync"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/pleo/util"

	log "github.com/sirupsen/logrus"
)

const (
	defaultClusterID   = "nafigos-cluster"
	defaultClientID    = "pleo"
	defaultConnAddress = "nats://localhost:4222"
)

var natsInfo map[string]string

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	natsInfo = common.GetNATSInfo(defaultClusterID, defaultClientID, defaultConnAddress)
}

func main() {
	var wg sync.WaitGroup
	wg.Add(4)
	go common.StreamingQueueSubscriber("Pleo.Run", "Pleo.RunQueue", util.Run, natsInfo, &wg)
	go common.StreamingQueueSubscriber("Pleo.ScaleUp", "Pleo.ScaleUpQueue", util.ScaleUp, natsInfo, &wg)
	go common.StreamingQueueSubscriber("Pleo.Delete", "Pleo.DeleteQueue", util.Delete, natsInfo, &wg)
	go common.SynchronousSubscriber("Pleo.CreateUserClusterKubeconfig", "Pleo.CreateUserClusterKubeconfigQueue", util.CreateUserClusterKubeconfig, natsInfo, &wg)
	wg.Wait()
}
