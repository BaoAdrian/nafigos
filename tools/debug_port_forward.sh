#!/bin/bash

if [ -z "$1" ]; then
    printf "Usage: $0 <nafigos service name>\n"
    exit 1
fi
DEBUG_PORT=56268

printf "Forwarding port $DEBUG_PORT for service $1\n"
POD=`kubectl get po --selector=service=$1 -o jsonpath='{.items[*].metadata.name}'`
kubectl port-forward pod/$POD $DEBUG_PORT:$DEBUG_PORT
