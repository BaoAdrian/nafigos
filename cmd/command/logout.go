package command

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

// LogoutCommand ...
type LogoutCommand struct {
	*BaseCommand
}

// Run ...
func (c *LogoutCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	flagSet.Parse(args)
	home := os.Getenv("HOME")
	errs := os.Remove(home + "/.nafigos/config.json")
	if errs != nil {
		fmt.Printf("No user is currently signed in\n")
		panic(errs)
	}
	fmt.Printf("Successfully logged out!\n")
	c.authToken = ""
	return 0
}

// Synopsis ...
func (c *LogoutCommand) Synopsis() string {
	return "Logs out of current user"
}

// Help ...
func (c *LogoutCommand) Help() string {
	helpText := `
	Usage: nafigos logout
	It is required that you use '-username <username>'
`
	return strings.TrimSpace(helpText)
}
