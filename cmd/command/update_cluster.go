package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/nafigos/common"
)

// UpdateClusterCommand ...
type UpdateClusterCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateClusterCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "name of cluster")
	defaultNamespace := flagSet.String("default_namespace", "", "default namespace to create resources in")
	config := flagSet.String("config", "", "base64 encoded kubeconfig")
	host := flagSet.String("host", "", "cluster host")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  defaultNamespace: '%s'\n  config: '%s'\n  host: '%s'\n", *filename, *name, *defaultNamespace, *config, *host)

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*name) > 0 || len(*defaultNamespace) > 0 || len(*config) > 0 || len(*host) > 0 {
			data = c.override(data, name, defaultNamespace, config, host)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(args) == 0 {
			c.UI.Error("Incorrect number of positional arguments (expected at least 1)")
			c.UI.Error(c.Help())
			return 1
		}

		data = []byte(fmt.Sprintf(`{
	"name": "%s",
	"default_namespace": "%s",
	"config": "%s",
	"host": "%s"
}`, *name, *defaultNamespace, *config, *host))

		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("PUT", "/clusters/"+args[0], "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateClusterCommand) Synopsis() string {
	return "update a cluster"
}

// Help ...
func (c *UpdateClusterCommand) Help() string {
	helpText := `
	Usage: nafigos update cluster [options] CLUSTERID

	Options:
		-f <filename>
			name of JSON file to read cluster information from
		-config <string>
			base64-encoded contents of a Kubeconfig file
		-name <string>
			name of the cluster
		-default_namespace <string>
			namespace to use if none is specified
		-host <string>
			hostname of the cluster
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateClusterCommand) override(data []byte, name *string, defaultNamespace *string, config *string, host *string) []byte {
	var clusterJSON common.Cluster
	json.Unmarshal(data, &clusterJSON)

	if len(*name) > 0 {
		clusterJSON.Name = *name
	}
	if len(*defaultNamespace) > 0 {
		clusterJSON.DefaultNamespace = *defaultNamespace
	}
	if len(*config) > 0 {
		clusterJSON.Config = *config
	}
	if len(*host) > 0 {
		clusterJSON.Host = *host
	}

	data, err := json.Marshal(clusterJSON)
	if err != nil {
		panic(err)
	}
	return data
}
