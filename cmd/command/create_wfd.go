package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

// CreateWfdCommand ...
type CreateWfdCommand struct {
	*BaseCommand
}

// CLIWfd ...
type CLIWfd struct {
	URL      string `json:"url"`
	Branch   string `json:"branch"`
	SecretID string `json:"secret_id"`
}

// Run ...
func (c *CreateWfdCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	url := flagSet.String("url", "", "url of git project")
	branch := flagSet.String("branch", "", "git branch (default master)")
	secretID := flagSet.String("secret", "", "ID of secret to authenticate to git with")
	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  url: '%s'\n  branch: '%s'\n  secretID: '%s'\n", *filename, *url, *branch, *secretID)

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*url) > 0 || len(*branch) > 0 || len(*secretID) > 0 {
			data = c.override(data, url, branch, secretID)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(*url) == 0 {
			c.UI.Error(c.Help())
			return 1
		}
		data = []byte(fmt.Sprintf(`{
	"url": "%s",
	"branch": "%s",
	"git_secret_id": "%s"
}`, *url, *branch, *secretID))

		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("POST", "/workflows", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateWfdCommand) Synopsis() string {
	return "create a new WorkflowDefinition"
}

// Help ...
func (c *CreateWfdCommand) Help() string {
	helpText := `
Usage: nafigos create wfd [options]

Of the following options, it is required that you use
either '-f <filename>' or utilize other options

Options:
	-f <filename>
  		name of json file to read user info from. Cannot be used with
		other options
	-url <string>
		the git URL to create WorkflowDefinition from
		(required if -f is not used)
	-branch <string>
		git branch to use (default master)
	-secret <string>
		(optional) the ID of the secret to use if auth is required
		access the git repository
`
	return strings.TrimSpace(helpText)
}

func (c *CreateWfdCommand) override(data []byte, url *string, branch *string, secretID *string) []byte {
	var wfdJSON CLIWfd
	json.Unmarshal(data, &wfdJSON)

	if len(*url) > 0 {
		wfdJSON.URL = *url
	}
	if len(*branch) > 0 {
		wfdJSON.Branch = *branch
	}
	if len(*secretID) > 0 {
		wfdJSON.SecretID = *secretID
	}

	data, err := json.Marshal(wfdJSON)
	if err != nil {
		panic(err)
	}
	return data
}
