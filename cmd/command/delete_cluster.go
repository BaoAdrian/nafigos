package command

import (
	"strings"
)

// DeleteClusterCommand ...
type DeleteClusterCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteClusterCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/clusters/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteClusterCommand) Synopsis() string {
	return "delete a cluster"
}

// Help ...
func (c *DeleteClusterCommand) Help() string {
	helpText := `
Usage: nafigos delete cluster [cluster ID]

Run 'nafigos get cluster' to view cluster ID'S			
`
	return strings.TrimSpace(helpText)
}
