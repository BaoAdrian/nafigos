package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// GetCommand ...
type GetCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *GetCommand) Synopsis() string {
	return "get information on a cluster, secret, user or Work Flow Definition "
}

// Help ...
func (c *GetCommand) Help() string {
	helpText := `
	Usage: nafigos get <subcommand>

`
	return strings.TrimSpace(helpText)
}
