package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// UpdateCommand ...
type UpdateCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *UpdateCommand) Synopsis() string {
	return "update a cluster, secret or user"
}

// Help ...
func (c *UpdateCommand) Help() string {
	helpText := `
	Usage: nafigos update <subcommand>

`
	return strings.TrimSpace(helpText)
}
