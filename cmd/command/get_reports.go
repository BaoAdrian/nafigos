package command

import (
	"strings"
)

// GetReportsCommand ...
type GetReportsCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetReportsCommand) Run(args []string) int {
	var wfdID string
	if len(args) > 0 {
		wfdID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/images"+wfdID, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetReportsCommand) Synopsis() string {
	return "get WorkflowDefinitions reports of the images used"
}

// Help ...
func (c *GetReportsCommand) Help() string {
	helpText := `
Usage: nafigos get reports [WFD ID]

	Gets the reports of all WFD's unless specific WFD ID is provided
`
	return strings.TrimSpace(helpText)
}
