package command

import (
	"strings"
)

// GetClusterCommand ...
type GetClusterCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetClusterCommand) Run(args []string) int {
	var clusterID string
	if len(args) > 0 {
		clusterID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/clusters"+clusterID, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetClusterCommand) Synopsis() string {
	return "get clusters"
}

// Help ...
func (c *GetClusterCommand) Help() string {
	helpText := `
Usage: nafigos get cluster [ID]

	Gets all clusters unless specific ID is provided
`
	return strings.TrimSpace(helpText)
}
