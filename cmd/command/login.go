package command

import (
	"flag"
	//"fmt"
	//"net/http"
	//"strconv"
	"strings"
)

// LoginCommand ...
type LoginCommand struct {
	*BaseCommand
}

// Run ...
func (c *LoginCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	flagSet.Parse(args)
	c.CheckConfiguration()
	return 0
}

// Synopsis ...
func (c *LoginCommand) Synopsis() string {
	return "Allows for login to Keycloak"
}

// Help ...
func (c *LoginCommand) Help() string {
	helpText := `
	Usage: nafigos login [options]
	It is required that you use '-username <username>'

Options:
	-username <string>
		username associated with the secret (required)
`
	return strings.TrimSpace(helpText)
}
