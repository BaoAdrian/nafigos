package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/nafigos/common"
)

// UpdateSecretCommand ...
type UpdateSecretCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateSecretCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	secretType := flagSet.String("type", "", "type of secret (gcr, dockerhub, or git)")
	secretUsername := flagSet.String("username", "", "username to use with secret")
	secretValue := flagSet.String("value", "", "value of the secret")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  secretType: '%s'\n  secretUsername: '%s'\n  secretValue: '%s'\n", *filename, *secretType, *secretUsername, *secretValue)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) < 2 {
		c.UI.Error("Incorrect number of positional arguments (expected 2)")
		c.UI.Error(c.Help())
		return 1
	}
	username := args[0]
	secretID := args[1]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*secretType) > 0 || len(*secretUsername) > 0 || len(*secretValue) > 0 {
			data = c.override(data, secretType, secretUsername, secretValue)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		data = []byte(fmt.Sprintf(`{
	"type": "%s",
	"username": "%s",
	"value": "%s"
}`, common.SecretType(*secretType), *secretUsername, *secretValue))

		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("PUT", fmt.Sprintf("/users/%s/secrets/%s", username, secretID), "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateSecretCommand) Synopsis() string {
	return "update a secret"
}

// Help ...
func (c *UpdateSecretCommand) Help() string {
	helpText := `
	Usage: nafigos update secret [options] USERNAME SECRETID

	Options:
		-f <filename>
			name of JSON file to read secret info from
		-type <string>
			type of secret (git, dockerhub, or gcr)
		-value <string>
			value of the secret (often a personal access token)
		-username <string>
			username to use with the secret
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateSecretCommand) override(data []byte, secretType *string, username *string, value *string) []byte {
	var secretJSON common.Secret
	json.Unmarshal(data, &secretJSON)

	if len(*secretType) > 0 {
		secretJSON.Type = common.SecretType(*secretType)
	}
	if len(*username) > 0 {
		secretJSON.Username = *username
	}
	if len(*value) > 0 {
		secretJSON.Value = *value
	}

	data, err := json.Marshal(secretJSON)
	if err != nil {
		panic(err)
	}
	return data
}
