package command

import (
	"strings"
)

// GetRunCommand ...
type GetRunCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetRunCommand) Run(args []string) int {
	var runID string
	if len(args) > 0 {
		runID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/runs"+runID, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetRunCommand) Synopsis() string {
	return "Get a run by ID.  Returns all runs for the current user if no ID is specified"
}

// Help ...
func (c *GetRunCommand) Help() string {
	helpText := `
Usage: nafigos get run [RUNID]

	Returns all runs if no run ID is specified
`
	return strings.TrimSpace(helpText)
}
